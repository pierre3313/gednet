delete from groups;

insert into groups(groupsid, description, grouptypeid) VALUES ('Administrateur', 'Groupe Administrateur', 'ADMIN');
insert into groups(groupsid, description, grouptypeid) VALUES ('Utilisateur', 'Groupe utilisateur', 'USER');
insert into groups(groupsid, description, grouptypeid) VALUES ('Invité', 'Groupe invité', 'READ');