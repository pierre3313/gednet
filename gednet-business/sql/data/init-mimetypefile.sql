DELETE FROM mimetypefile;

INSERT INTO mimetypefile (mimetypefileid, mimetypefilename)
	VALUES 
	('application/pdf', 'Fichier PDF'),
	('application/vnd.adobe.x-mars', 'Fichier PDF'),
	('application/msword', 'Fichier DOC'),
	('application/vnd.ms-excel', 'Fichier XLS'),
	('application/vnd.ms-powerpoint', 'Fichier PPT'),
	('application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'Fichier DOCX'),
	('application/vnd.openxmlformats-officedocument .spreadsheetml.sheet', 'Fichier XLSX'),
	('application/vnd.openxmlformats-officedocument.presentationml.presentation', 'Fichier PPTX'),
	('application/vnd.oasis.opendocument.text', 'Fichier OPEN DOCUMENT'),
	('application/vnd.oasis.opendocument.spreadsheet', 'Fichier CALCUL'),
	('application/vnd.oasis.opendocument.presentation', 'Fichier PRESENTATION'),
	('application/vnd.oasis.opendocument.graphics', 'Fichier GRAPHIC');