CREATE USER gednet WITH PASSWORD 'gednet' 
NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;

COMMENT ON ROLE gednet IS 'Pour acc�s au schéma gednet';

CREATE SCHEMA gednet
  AUTHORIZATION gednet;
COMMENT ON SCHEMA gednet
  IS 'schéma pour appli gednet';

GRANT CREATE ON TABLESPACE defaulttbs TO gednet;

GRANT ALL ON SCHEMA gednet TO gednet;