
/* Création sequence et table dossier */

drop sequence folder_folderid_sequence;
drop table folder;

create sequence folder_folderid_sequence 
increment by 1 
start with 1;

CREATE TABLE folder(
	"folderid" int NOT NULL,
	"level" int NOT NULL,
	"fleft" int NOT NULL,
	"fright" int NOT NULL,
	"name" VARCHAR(128) NOT NULL,
	"created" TIMESTAMP,
	PRIMARY KEY  ("folderid")
);

ALTER TABLE folder ALTER COLUMN "folderid" SET DEFAULT nextval('folder_folderid_sequence');
ALTER TABLE folder ALTER COLUMN "folderid" SET NOT NULL;
ALTER SEQUENCE folder_folderid_sequence OWNED BY folder."folderid";

/* Création sequence et table mimetypefile */

drop table mimetypefile;

CREATE TABLE mimetypefile(
	"mimetypefileid" VARCHAR(128),
	"mimetypefilename" VARCHAR(128),
	PRIMARY KEY  ("mimetypefileid")
);

/* Création sequence et table file */
drop sequence file_fileid_sequence;
drop table file;

create sequence file_fileid_sequence 
increment by 1 
start with 1;

CREATE TABLE file(
	"fileid" int NOT NULL,
	"versionfileid" int NOT NULL,
	"versionfile" NUMERIC(5, 2),	
	"name" VARCHAR(128),
	"size" int,
	"created" TIMESTAMP,
	"lastmodified" TIMESTAMP,
	"mimetypefileid" VARCHAR(128),
	"folderid" int NOT NULL,
	PRIMARY KEY  ("fileid")
);

ALTER TABLE file ALTER COLUMN "fileid" SET DEFAULT nextval('file_fileid_sequence');
ALTER TABLE file ALTER COLUMN "fileid" SET NOT NULL;
ALTER SEQUENCE file_fileid_sequence OWNED BY file."fileid";

create sequence file_versionfileid_sequence 
increment by 1 
start with 1;

/* Création sequence et table file */
drop sequence filecontent_filecontentid_sequence;
drop table filecontent;

create sequence filecontent_filecontentid_sequence 
increment by 1 
start with 1;

CREATE TABLE filecontent(
	"filecontentid" int NOT NULL,
	"content" bytea,
	"fileid" int NOT NULL,
	PRIMARY KEY  ("filecontentid")
);

ALTER TABLE filecontent ALTER COLUMN "filecontentid" SET DEFAULT nextval('filecontent_filecontentid_sequence');
ALTER TABLE filecontent ALTER COLUMN "filecontentid" SET NOT NULL;
ALTER SEQUENCE filecontent_filecontentid_sequence OWNED BY filecontent."filecontentid";

/* Création sequence et table fileactivity */
drop table fileactivity;
drop sequence fileactivity_fileactivityid_sequence;

create sequence fileactivity_fileactivityid_sequence 
increment by 1 
start with 1;

CREATE TABLE fileactivity(
	"fileactivityid" int NOT NULL,
	"created" TIMESTAMP,
	"event" VARCHAR(256),
	"fileid" int NOT NULL,
	"usersid" VARCHAR(256),
	PRIMARY KEY  ("fileactivityid")
);

ALTER TABLE fileactivity ALTER COLUMN "fileactivityid" SET DEFAULT nextval('fileactivity_fileactivityid_sequence');
ALTER TABLE fileactivity ALTER COLUMN "fileactivityid" SET NOT NULL;
ALTER SEQUENCE fileactivity_fileactivityid_sequence OWNED BY fileactivity."fileactivityid";

/* Création sequence et table filetags */
drop sequence filetags_filetagsid_sequence;
drop table filetags;

create sequence filetags_filetagsid_sequence 
increment by 1 
start with 1;

CREATE TABLE filetags(
	"filetagsid" int NOT NULL,
	"created" TIMESTAMP,
	"fileid" int NOT NULL,
	"tag" VARCHAR(32),
	PRIMARY KEY  ("filetagsid")
);

ALTER TABLE filetags ALTER COLUMN "filetagsid" SET DEFAULT nextval('filetags_filetagsid_sequence');
ALTER TABLE filetags ALTER COLUMN "filetagsid" SET NOT NULL;
ALTER SEQUENCE filetags_filetagsid_sequence OWNED BY filetags."filetagsid";


/* Création sequence et table grouptype */
drop table grouptype;

CREATE TABLE grouptype(
	"grouptypeid" VARCHAR(16),
	"name" VARCHAR(64),
	PRIMARY KEY  ("grouptypeid")
);

/* Création sequence et table group */
drop table groups;

CREATE TABLE groups(
	"groupsid" VARCHAR(64),
	"description" VARCHAR(128),
	"grouptypeid" VARCHAR(16),
	PRIMARY KEY  ("groupsid")
);

/* Création sequence et table users */
drop table users;
drop sequence users_usersid_sequence;

CREATE TABLE users(
    "usersid" VARCHAR(256),
	"name" VARCHAR(128),
	"firstname" VARCHAR(128),
	"password" VARCHAR(256),
	PRIMARY KEY  ("usersid")
);

/* Création table assignment => association entre la table groups et la table users*/
drop table assignment;

CREATE TABLE assignment(
    "usersid" VARCHAR(256),
    "groupsid" VARCHAR(64)
);

/* Création sequence et table token */
drop table token;

CREATE TABLE token(
	"tokenid" VARCHAR(256),
	"salt" VARCHAR(64),
	"refreshtoken" VARCHAR(256),
	"saltrefresh" VARCHAR(64),
	"usersid" VARCHAR(256),
	PRIMARY KEY  ("tokenid")
);

