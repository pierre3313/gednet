DELETE FROM mimetypefile;

INSERT INTO mimetypefile (mimetypefileid, mimetypefilename)
	VALUES 
	('application/pdf', 'Fichier PDF'),
	('application/vnd.adobe.x-mars', 'Fichier PDF'),
	('application/msword', 'Fichier WORD DOC'),
	('application/vnd.ms-excel', 'Fichier EXCEL XLS'),
	('application/vnd.ms-powerpoint', 'Fichier POWER POINT PPT'),
	('application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'Fichier WORD DOCX'),
	('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'Fichier EXCEL XLSX'),
	('application/vnd.openxmlformats-officedocument.presentationml.presentation', 'Fichier POWER POINT PPTX'),
	('application/vnd.oasis.opendocument.text', 'Fichier OPEN DOCUMENT'),
	('application/vnd.oasis.opendocument.spreadsheet', 'Fichier CALCUL'),
	('application/vnd.oasis.opendocument.presentation', 'Fichier PRESENTATION'),
	('application/vnd.oasis.opendocument.graphics', 'Fichier GRAPHIC');
	