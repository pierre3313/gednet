DELETE FROM folder;

INSERT INTO folder (level, fleft, fright, name) 
    VALUES 
        (0, 1, 37, 'Dossier 0'),
        (1, 2, 27, 'Dossier 1'),
        (2, 3, 24, 'Dossier 1.1'),
        (3, 4, 23, 'Dossier 1.1.1'),
        (4, 5, 18, 'Dossier 1.1.1.1'),
        (5, 6, 13, 'Dossier 1.1.1.1.1.1'),
        (6, 7, 8, 'Dossier 1.1.1.1.1.1.1'),
        (6, 9, 10, 'Dossier 1.1.1.1.1.1.2'),
        (6, 11, 12, 'Dossier 1.1.1.1.1.1.3'),
        (5, 14, 15, 'Dossier 1.1.1.1.1.2'),
        (5, 16, 17, 'Dossier 1.1.1.1.1.3'),
        (4, 19, 20, 'Dossier 1.1.1.1.2'),
        (4, 21, 22, 'Dossier 1.1.1.1.3'),
        (3, 25, 26, 'Dossier 1.1.2'),
        (2, 27, 28, 'Dossier 1.2'),
        (2, 29, 30, 'Dossier 1.3'),
        (2, 31, 36, 'Dossier 1.4'),
        (3, 32, 33, 'Dossier 1.4.1'),
        (3, 34, 35, 'Dossier 1.4.2');