package fr.gednet.util;

public class MimeTypeUtil {
	
	public static String identifyMimeTypeByExtension(String filename){
		
		String extension = null;
		try{
			
			extension = filename.substring(filename.lastIndexOf(".") + 1, filename.length()).toLowerCase();
			
		}catch(Exception e){
			
		}
		
		String mimeTypeDetected = null;
		switch(extension){
			case "pdf":
				mimeTypeDetected = "application/pdf";
				break;
			default : 
				mimeTypeDetected = null;
		}
		
		return mimeTypeDetected;
	}

}
