package fr.gednet.bp;

import javax.crypto.spec.SecretKeySpec;
import javax.inject.Inject;
import javax.ws.rs.NotAuthorizedException;
import javax.xml.bind.DatatypeConverter;

import fr.gednet.util.SecurityUtil;
import io.jsonwebtoken.*;
import org.omg.CORBA.UnknownUserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gednet.bp.persistence.Token;
import fr.gednet.bp.persistence.User;
import fr.gednet.bp.persistence.manager.GroupsPersistenceManager;
import fr.gednet.bp.persistence.manager.TokenPersistenceManager;
import fr.gednet.bp.persistence.manager.UsersPersistenceManager;
import fr.gednet.rs.ret.Credentials;

import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Timestamp;
import java.util.Date;

public class SecureBusinessProcess {
	
	private static Logger LOGGER = LoggerFactory.getLogger(SecureBusinessProcess.class);

	public static long TOKEN_EXPIRATION = 1200000;
    public static long REFRESH_TOKEN_EXPIRATION = 12000000;

	@Inject
	UsersPersistenceManager usersPersistenceManager;
	
	@Inject
	TokenPersistenceManager tokenPersistenceManager;


    /**
     * Méthode d'authentification
     * @param userid
     * @param password
     * @throws InvalidKeySpecException
     * @throws NoSuchAlgorithmException
     * @throws NotAuthorizedException
     * @throws UnknownUserException
     */
    public void authenticateUsers(String userid, String password) throws InvalidKeySpecException, NoSuchAlgorithmException, NotAuthorizedException, UnknownUserException {

        User users = null;
        boolean isPasswordValid = false;

        if(userid != null && password != null){
            users = usersPersistenceManager.findUsersByUsersid(
                    userid);
        }

        if(users != null)
            isPasswordValid = SecurityUtil.validatePassword(password, users.getPassword());

        if(!isPasswordValid)
            throw new UnknownUserException();
    }

    /**
     * Méthode de génération du token
     * @param userid
     * @return
     */
    public String issueToken(String userid) throws UnknownUserException, NoSuchAlgorithmException {

        User users = usersPersistenceManager.findUsersByUsersid(userid);

        if(users == null)
            throw new UnknownUserException();

        //Génération de la clé secrete pour chriffrement jeton
        String salt = SecurityUtil.toHex(SecurityUtil.getSalt());

        String tokenChain = SecurityUtil.buildJwtBuilder(
                salt,
                users.getUsersid(),
                users.getName() + " " + users.getFirstname(),
                users.getUsersid(),
                SecureBusinessProcess.TOKEN_EXPIRATION).compact();

        String saltRefresh = SecurityUtil.toHex(SecurityUtil.getSalt());

        String refreshTokenChain = SecurityUtil.buildJwtBuilder(
                saltRefresh,
                users.getUsersid(),
                users.getName() + " " + users.getFirstname(),
                users.getUsersid(),
                SecureBusinessProcess.REFRESH_TOKEN_EXPIRATION).compact();

        //Verification et suppression token existant
        Token token = tokenPersistenceManager.findTokenByUsersId(users.getUsersid());
        if(token != null)
            tokenPersistenceManager.removeToken(token.getTokenid());

        //Création d'un token persistant
        token = new Token();

        token.setTokenid(tokenChain);
        token.setSalt(salt);
        token.setRefreshtoken(refreshTokenChain);
        token.setSaltrefresh(saltRefresh);
        token.setUsersid(users.getUsersid());

        //Persiste token
        tokenPersistenceManager.addToken(token);

        return token.getTokenid();
    }

    /**
     * Méthode de validation du token
     * @param tokenid
     * @return
     */
    public boolean validateToken(String tokenid) throws MalformedJwtException, NoSuchAlgorithmException, UnknownUserException {

        boolean isValidToken = false;

        Token token = tokenPersistenceManager.findTokenByTokenId(tokenid);

        if(token == null)
            throw new NotAuthorizedException("Token inconnu");

        try{

            //Décodage et Récupération des éléments du jeton
            Claims claims = Jwts.parser()
                    .setSigningKey(DatatypeConverter.parseBase64Binary(token.getSalt()))
                    .parseClaimsJws(tokenid).getBody();

            isValidToken = true;

            if(LOGGER.isDebugEnabled())
                LOGGER.debug("Jeton : " + claims.toString());

            if(LOGGER.isDebugEnabled())
                LOGGER.debug("Jeton valide : " + claims.toString());

        }catch(ExpiredJwtException e){

        }

        return isValidToken;
    }

    /**
     * Récupére les éléments d'un USER en fonction du token d'auhtentification
     * @param tokenid
     * @return
     */
    public User getUserAuthenticated(String tokenid){

        Token token = tokenPersistenceManager.findTokenByTokenId(tokenid);
        User user = usersPersistenceManager.findUsersByUsersid(token.getUsersid());

        return user;
    }

}
