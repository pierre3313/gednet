package fr.gednet.bp.persistence;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="folder")
public class Folder implements Serializable{

	/**
	 * Serial folder version
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="folderid")
	@SequenceGenerator(name = "job_folderid_seq", sequenceName="folder_folderid_sequence", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "job_folderid_seq")
	private int folderid;
	
	@Column(name="level")
	private int level;
	
	@Column(name="fleft")
	private int fleft;
	
	@Column(name="fright")
	private int fright;
	
	@Column(name="name")
	private String name;
	
	@Column(name="created")
	private Timestamp created;
		
	public Folder() {
		super();
	}

	public Folder(int level, int fleft, int fright, String name) {
		super();
		this.level = level;
		this.fleft = fleft;
		this.fright = fright;
		this.name = name;
	}

	public int getFolderid() {
		return folderid;
	}

	public void setFolderid(int folderid) {
		this.folderid = folderid;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}
	
	public int getFleft() {
		return fleft;
	}

	public void setFleft(int fleft) {
		this.fleft = fleft;
	}

	public int getFright() {
		return fright;
	}

	public void setFright(int fright) {
		this.fright = fright;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	@Override
	public String toString() {
		return "Folder [folderid=" + folderid + ", level=" + level + ", fleft="
				+ fleft + ", fright=" + fright + ", name=" + name + "]";
	}
}
