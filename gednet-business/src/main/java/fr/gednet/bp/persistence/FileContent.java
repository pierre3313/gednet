package fr.gednet.bp.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="filecontent")
public class FileContent implements Serializable {
	
	/**
	 * 	Serial FileContent version
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="filecontentid")
	@SequenceGenerator(name = "job_filecontentid_seq", sequenceName="filecontent_filecontentid_sequence", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "job_filecontentid_seq")
	private int filecontentid;
	
	@Column(name="content")
	private byte[] content;
	
	@Column(name="fileid")
	private int fileid;

	public int getFilecontentid() {
		return filecontentid;
	}

	public void setFilecontentid(int filecontentid) {
		this.filecontentid = filecontentid;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public int getFileid() {
		return fileid;
	}

	public void setFileid(int fileid) {
		this.fileid = fileid;
	}

	@Override
	public String toString() {
		return "FileContent [filecontentid=" + filecontentid + ", content="
				+ "[NOT VISIBLE]" + ", fileid=" + fileid + "]";
	}
}
