package fr.gednet.bp.persistence.manager;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import fr.gednet.bp.persistence.Groups;
import fr.gednet.bp.persistence.db.GednetDatabase;

public class GroupsPersistenceManager {

	@Inject
	@GednetDatabase
	private transient EntityManager m_entityManager;
	
	/**
	 * Recherche tous les groups 
	 * @return
	 */
	public List<Groups> findGroups(){
		
		return m_entityManager.createQuery("select g from Groups as g", Groups.class)
				.getResultList();
		
	}
	
	/**
	 * Recherche d'un groupe en fonction de son id
	 * @param groupsid
	 * @return
	 */
	public Groups findGroupsByGroupsid(int groupsid){
		
		return m_entityManager.createQuery("select g from Groups as g "
				+ "where g.groupsid = :groupsid ", Groups.class)
				.getSingleResult();
		
	}
	
	/**
	 * Ajout d'un groupe
	 * @param groups
	 * @return
	 */
	public Groups addGroups(Groups groups){
		
		m_entityManager.persist(groups);
		
		return groups;
	}
	
	/**
	 * Suppression d'un groupe
	 * @param groupsid
	 */
	public void removeGroups(int groupsid){
		
		Groups groups = m_entityManager.find(Groups.class, groupsid);
				
		m_entityManager.remove(groups);
	}
	
}
