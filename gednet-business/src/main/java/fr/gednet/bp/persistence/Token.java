package fr.gednet.bp.persistence;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="token")
public class Token implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="tokenid")
	private String tokenid;

    @Column(name="salt")
    private String salt;

	@Column(name="refreshtoken")
	private String refreshtoken;

    @Column(name="saltrefresh")
    private String saltrefresh;
	
	@Column(name="usersid")
	private String usersid;

    public String getTokenid() {
        return tokenid;
    }

    public void setTokenid(String tokenid) {
        this.tokenid = tokenid;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getRefreshtoken() {
        return refreshtoken;
    }

    public void setRefreshtoken(String refreshtoken) {
        this.refreshtoken = refreshtoken;
    }

    public String getSaltrefresh() {
        return saltrefresh;
    }

    public void setSaltrefresh(String saltrefresh) {
        this.saltrefresh = saltrefresh;
    }

    public String getUsersid() {
        return usersid;
    }

    public void setUsersid(String usersid) {
        this.usersid = usersid;
    }

    @Override
    public String toString() {
        return "Token{" +
                "tokenid='" + tokenid + '\'' +
                ", salt='" + salt + '\'' +
                ", refreshtoken='" + refreshtoken + '\'' +
                ", saltrefresh='" + saltrefresh + '\'' +
                ", usersid='" + usersid + '\'' +
                '}';
    }
}
