package fr.gednet.bp.persistence.manager;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import fr.gednet.bp.persistence.User;
import fr.gednet.bp.persistence.db.GednetDatabase;

public class UsersPersistenceManager {
	
	@Inject
	@GednetDatabase
	private transient EntityManager m_entityManager;
	
	/**
	 * Recherche tous les Users 
	 * @return
	 */
	public List<User> findUsers(){
		
		return m_entityManager.createQuery("select u from User as u", User.class)
				.getResultList();
		
	}
	
	/**
	 * Recherche d'un Users en fonction de son id
	 * @param usersid
	 * @return
	 */
	public User findUsersByUsersid(String usersid){
		try{
			
			return m_entityManager.createQuery("select u from User as u "
					+ "where u.usersid = :usersid ", User.class)
					.setParameter("usersid", usersid)
					.getSingleResult();
			
		}catch(NoResultException nre){
			return null;
		}
	}
	
	/**
	 * Recherche d'un Users en fonction du groupid
	 * @param groupsid
	 * @return
	 */
	public List<User> findUsersByGroupsid(int groupsid){
		
		return m_entityManager.createQuery("select u from User as u "
				+ "where u.groupsid = :groupsid ", User.class)
				.setParameter("groupsid", groupsid)
				.getResultList();
		
	}
	
	/**
	 * Ajout d'un Users
	 * @param users
	 * @return
	 */
	public User addUsers(User users){
		
		m_entityManager.persist(users);
		
		return users;
	}
	
	/**
	 * Suppression d'un Users
	 * @param groupsid
	 */
	public void removeUsers(int usersid){
		
		User users = m_entityManager.find(User.class, usersid);
				
		m_entityManager.remove(users);
	}
	
	/**
	 * mise à jour d'un Users
	 * @param users
	 * @return
	 */
	public User updateUsers(User users){
		
		return m_entityManager.merge(users);
	}
}
