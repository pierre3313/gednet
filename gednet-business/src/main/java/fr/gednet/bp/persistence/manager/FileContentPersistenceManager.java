package fr.gednet.bp.persistence.manager;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import fr.gednet.bp.persistence.FileContent;
import fr.gednet.bp.persistence.db.GednetDatabase;

/**
 * Manager de persistance pour les objets File
 * @author epso8080
 *
 */
public class FileContentPersistenceManager {
	
	@Inject
	@GednetDatabase
	private transient EntityManager m_entityManager;
	
	/**
	 * Recherche du contenu d'un fichier en fonction du fileId
	 * @param folderid
	 * @return
	 */
	public FileContent findFileContentByFileId(int fileid){
		
		return m_entityManager.createQuery("select fc from FileContent as fc "
				+ "where fc.fileid = :fileid ", FileContent.class)
				.setParameter("fileid", fileid)
				.getSingleResult();
		
	}	
	
	/**
	 * Ajout du contenu d'un fichier
	 * @param file
	 * @return
	 */
	public FileContent addFileContent(FileContent fileContent){
		
		m_entityManager.persist(fileContent);
		
		return fileContent;
	}
	
	/**
	 * Suppression du contenu d'un fichier
	 * @param fileid
	 * @return
	 */
	public void removeFileContent(int fileid){
		
		FileContent fileContent = findFileContentByFileId(fileid);
				
		m_entityManager.remove(fileContent);
	}
	
	/**
	 * mise à jour du contenu d'un fichier
	 * @param file
	 * @return
	 */
	public FileContent updateFile(FileContent fileContent){
		
		return m_entityManager.merge(fileContent);
	}

}
