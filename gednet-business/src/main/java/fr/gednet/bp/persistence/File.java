package fr.gednet.bp.persistence;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="file")
public class File implements Serializable {

	/**
	 * Serial File version
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="fileid")
	@SequenceGenerator(name = "job_fileid_seq", sequenceName="file_fileid_sequence", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "job_fileid_seq")
	private int fileid;
	
	@Column(name="versionfileid")
	private int versionfileid;
	
	@Column(name="versionfile")
	private float versionfile;
	
	@Column(name="name")
	private String name;
	
	@Column(name="size")
	private long size;
	
	@Column(name="created")
	private Timestamp created;
	
	@Column(name="lastmodified")
	private Timestamp lastmodified;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="mimetypefileid")
	private MimeTypeFile mimetypefile;
	
	@Column(name="folderid")
	private int folderid;

	public File(int fileid, String name, int size, String mimetypefileid,
			int folderid) {
		super();
		this.fileid = fileid;
		this.name = name;
		this.size = size;
		this.folderid = folderid;
	}

	public File() {
		super();
	}
	
	public int getFileid() {
		return fileid;
	}

	public void setFileid(int fileid) {
		this.fileid = fileid;
	}

	public int getVersionfileid() {
		return versionfileid;
	}

	public void setVersionfileid(int versionfileid) {
		this.versionfileid = versionfileid;
	}

	public float getVersionfile() {
		return versionfile;
	}

	public void setVersionfile(float versionfile) {
		this.versionfile = versionfile;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getLastmodified() {
		return lastmodified;
	}

	public void setLastmodified(Timestamp lastmodified) {
		this.lastmodified = lastmodified;
	}
	
	public MimeTypeFile getMimetypefile() {
		return mimetypefile;
	}

	public void setMimetypefile(MimeTypeFile mimetypefile) {
		this.mimetypefile = mimetypefile;
	}

	public int getFolderid() {
		return folderid;
	}

	public void setFolderid(int folderid) {
		this.folderid = folderid;
	}

	@Override
	public String toString() {
		return "File [fileid=" + fileid + ", versionfileid=" + versionfileid
				+ ", versionfile=" + versionfile + ", name=" + name + ", size="
				+ size + ", created=" + created + ", lastmodified="
				+ lastmodified + ", mimetypefile=" + mimetypefile
				+ ", folderid=" + folderid + "]";
	}	
}
