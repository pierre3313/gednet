package fr.gednet.bp.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="grouptype")
public class GroupType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="grouptypeid")
	private String grouptypeid;

	@Column(name="grouptype")
	private String grouptype;

	public String getGrouptypeid() {
		return grouptypeid;
	}
	public void setGrouptypeid(String grouptypeid) {
		this.grouptypeid = grouptypeid;
	}
	public String getGrouptype() {
		return grouptype;
	}
	public void setGrouptype(String grouptype) {
		this.grouptype = grouptype;
	}

	@Override
	public String toString() {
		return "GroupType{" +
				"grouptypeid='" + grouptypeid + '\'' +
				", grouptype='" + grouptype + '\'' +
				'}';
	}
}
