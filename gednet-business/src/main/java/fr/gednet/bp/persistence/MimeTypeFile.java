package fr.gednet.bp.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="mimetypefile")
public class MimeTypeFile implements Serializable{

	
	/**
	 * Serial MimeTypeFile version
	 */
	private static final long serialVersionUID = 1L;
		
	@Id
	@Column(name="mimetypefileid")
	private String mimetypefileid;
	
	@Column(name="mimetypefilename")
	private String mimetypefilename;
	
	public MimeTypeFile(String mimetypefileid, String mimetypefilename) {
		super();
		this.mimetypefileid = mimetypefileid;
		this.mimetypefilename = mimetypefilename;
	}

	public MimeTypeFile() {
		super();
	}

	public String getMimetypefileid() {
		return mimetypefileid;
	}

	public void setMimetypefileid(String mimetypefileid) {
		this.mimetypefileid = mimetypefileid;
	}

	public String getMimetypefilename() {
		return mimetypefilename;
	}

	public void setMimetypefilename(String mimetypefilename) {
		this.mimetypefilename = mimetypefilename;
	}

	@Override
	public String toString() {
		return "MimeTypeFile [mimetypefileid=" + mimetypefileid
				+ ", mimetypefilename=" + mimetypefilename + "]";
	}
}
