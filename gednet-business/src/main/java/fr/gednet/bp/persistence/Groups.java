package fr.gednet.bp.persistence;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="groups")
public class Groups implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="groupsid")
	private String groupsid;
	
	@Column(name="description")
	private String description;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="grouptypeid")
    private GroupType groupType;

    @ManyToMany(mappedBy="groups")
	List<User> users;

	public String getGroupsid() {
		return groupsid;
	}
	public void setGroupsid(String groupsid) {
		this.groupsid = groupsid;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public GroupType getGroupType() {
        return groupType;
    }
    public void setGroupType(GroupType groupType) {
        this.groupType = groupType;
    }
    public List<User> getUsers() {
        return users;
    }
    public void setUsers(List<User> users) {
        this.users = users;
    }

	@Override
	public String toString() {
		return "Groups{" +
				"groupsid=" + groupsid +
				", description='" + description + '\'' +
				", groupType=" + groupType +
				", users=" + users +
				'}';
	}
}
