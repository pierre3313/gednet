package fr.gednet.bp.persistence.manager;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import fr.gednet.bp.persistence.FileActivity;
import fr.gednet.bp.persistence.db.GednetDatabase;

public class FileActivityPersistenceManager {
	
	@Inject
	@GednetDatabase
	private transient EntityManager m_entityManager;
	
	/**
	 * Recherche les activités attachées au fichier
	 * @param folderid
	 * @return
	 */
	public List<FileActivity> findFileActivityByFileid(int fileid){
		
		return m_entityManager.createQuery("select fa from FileActivity as fa "
				+ "where fa.fileid = :fileid ", FileActivity.class)
				.setParameter("fileid", fileid)
				.getResultList();
		
	}
	
	/**
	 * Ajout d'une activité sur un fichier
	 * @param file
	 * @return
	 */
	public FileActivity addFileActivity(FileActivity fileActivity){
		
		m_entityManager.persist(fileActivity);
		
		return fileActivity;
	}
	
	/**
	 * Suppression d'une activité
	 * @param fileid
	 * @return
	 */
	public void removeFileActivity(int fileactivityid){
		
		FileActivity fileActivity = m_entityManager.find(FileActivity.class, fileactivityid);
				
		m_entityManager.remove(fileActivity);
	}
}
