package fr.gednet.bp.persistence.manager;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import fr.gednet.bp.persistence.File;
import fr.gednet.bp.persistence.db.GednetDatabase;

/**
 * Manager de persistance pour les objets File
 * @author epso8080
 *
 */
public class FilePersistenceManager {
	
	@Inject
	@GednetDatabase
	private transient EntityManager m_entityManager;
	
	/**
	 * Recherche tous les Fichiers pour un dossier
	 * @return
	 */
	public List<File> findAllFilesByFolderId(int folderid){
		
		return m_entityManager.createQuery("select f from File as f "
				+ "where f.folderid = :folderid "
				+ "order by f.name asc", File.class)
				.setParameter("folderid", folderid)
				.getResultList();
		
    }

	/**
	 * Recherche un fichier en fonction de son id
	 * @param fileid
	 * @return
	 */
	public File findFile(int fileid){
		
		return m_entityManager.find(File.class, fileid);
		
	}	
	
	/**
	 * Ajout d'un fichier
	 * @param file
	 * @return
	 */
	public File addFile(File file){
		
		m_entityManager.persist(file);
		
		return file;
	}
	
	/**
	 * Récupère la dernière valeur d'id de version
	 * @return
	 */
	public int getVersionIdFile(){
		
		long num = (long)m_entityManager.createNativeQuery("SELECT nextval('file_versionfileid_sequence') as num").getSingleResult();
		
		return (int)num;
	}
	
	/**
	 * Suppression d'un fichier
	 * @param fileid
	 * @return
	 */
	public File removeFile(int fileid){
		
		File file = m_entityManager.find(File.class, fileid);
				
		m_entityManager.remove(file);
		
		return file;
	}
	
	/**
	 * mise à jour d'un fichier
	 * @param file
	 * @return
	 */
	public File updateFile(File file){
		
		return m_entityManager.merge(file);
	}

}
