package fr.gednet.bp.persistence;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="filetags")
public class FileTags implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="filetagsid")
	@SequenceGenerator(name = "job_filetagsid_seq", sequenceName="filetags_filetagsid_sequence", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "job_filetagsid_seq")
	private int filetagsid;
	
	@Column(name="created")
	private Timestamp created;
	
	@Column(name="fileid")
	private int fileid;
	
	@Column(name="tag")
	private String tag;

	public int getFiletagsid() {
		return filetagsid;
	}
	public void setFiletagsid(int filetagsid) {
		this.filetagsid = filetagsid;
	}
	public Timestamp getCreated() {
		return created;
	}
	public void setCreated(Timestamp created) {
		this.created = created;
	}	
	public int getFileid() {
		return fileid;
	}
	public void setFileid(int fileid) {
		this.fileid = fileid;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	
	@Override
	public String toString() {
		return "FileTags [filetagsid=" + filetagsid + ", created=" + created
				+ ", fileid=" + fileid + ", tag=" + tag + "]";
	}
}
