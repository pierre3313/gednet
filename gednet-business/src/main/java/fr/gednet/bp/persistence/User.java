package fr.gednet.bp.persistence;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="users")
public class User implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="usersid")
	private String usersid;
	
	@Column(name="name")
	private String name;
	
	@Column(name="firstname")
	private String firstname;
	
	@Column(name="password")
	private String password;

    @ManyToMany
    @JoinTable(
            name="assignment"
            , joinColumns={
            @JoinColumn(name="usersid",
					referencedColumnName = "usersid")
        }
            , inverseJoinColumns={
            @JoinColumn(name="groupsid",
					referencedColumnName = "groupsid")
        }
    )
	private List<Groups> groups;

    public String getUsersid() {
        return usersid;
    }
    public void setUsersid(String usersid) {
        this.usersid = usersid;
    }
    public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
    public List<Groups> getGroups() {
        return groups;
    }
    public void setGroups(List<Groups> groups) {
        this.groups = groups;
    }

    @Override
    public String toString() {
        return "User{" +
                "usersid='" + usersid + '\'' +
                ", name='" + name + '\'' +
                ", firstname='" + firstname + '\'' +
                ", password='" + password + '\'' +
                ", groups=" + groups +
                '}';
    }
}
