package fr.gednet.bp.persistence.manager;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import fr.gednet.bp.persistence.FileTags;
import fr.gednet.bp.persistence.db.GednetDatabase;

public class FileTagsPersistenceManager {
	
	@Inject
	@GednetDatabase
	private transient EntityManager m_entityManager;
	
	/**
	 * Recherche les tags attachés au fichier
	 * @param folderid
	 * @return
	 */
	public List<FileTags> findFileTagsByFileid(int fileid){
		
		return m_entityManager.createQuery("select ft from FileTags as ft "
				+ "where ft.fileid = :fileid ", FileTags.class)
				.setParameter("fileid", fileid)
				.getResultList();
		
	}
	
	/**
	 * Ajout d'un tag
	 * @param file
	 * @return
	 */
	public FileTags addFileTags(FileTags fileTags){
		
		m_entityManager.persist(fileTags);
		
		return fileTags;
	}
	
	/**
	 * Suppression d'un tag
	 * @param fileid
	 * @return
	 */
	public void removeFileTags(int filetagsid){
		
		FileTags fileTags = m_entityManager.find(FileTags.class, filetagsid);
				
		m_entityManager.remove(fileTags);
	}
	
	/**
	 * Suppression des tags d'un fichier
	 * @param fileid
	 * @return
	 */
	public void removeFileTagsByFileid(int fileid){
		
		m_entityManager.createQuery("delete from FileTags where fileid = :fileid").setParameter("fileid", fileid).executeUpdate();
				
		
	}

}
