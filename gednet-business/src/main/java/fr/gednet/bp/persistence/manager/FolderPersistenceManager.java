package fr.gednet.bp.persistence.manager;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import fr.gednet.bp.persistence.Folder;
import fr.gednet.bp.persistence.db.GednetDatabase;

/**
 * Manager de persistance pour les objets Folder
 * @author epso8080
 *
 */
public class FolderPersistenceManager {
	
	@Inject
	@GednetDatabase
	private transient EntityManager m_entityManager;
	
	/**
	 * Recherche tous les dossiers en base
	 * @return
	 */
	public List<Folder> findAllFolders(){
		
		return m_entityManager.createQuery("select f from Folder as f "
				+ "where f.level > 0 "
				+ "order by f.fleft, f.name asc", Folder.class)
				.getResultList();
		
    }
	
	/**
	 * Recherche un dossier en fonction de son id
	 * @param folderid
	 * @return
	 */
	public Folder findFolder(int folderid){
		
		return m_entityManager.find(Folder.class, folderid);
		
	}
	
	/**
	 * Recherche les parents pour un dossier
	 * @param folderid
	 * @return
	 */
	public List<Folder> findParentsFolders(int folderid){
		
		Folder folder = m_entityManager.find(Folder.class, folderid);
		
		return m_entityManager.createQuery("select f from Folder as f "
				+ "where f.fleft <= :fleft and f.fright >= :fright and f.level > 0 "
				+ "order by f.fleft asc", Folder.class)
				.setParameter("fleft", folder.getFleft())
				.setParameter("fright", folder.getFright())
				.getResultList();
	}
	
	/**
	 * Recherche les parents pour un dossier
	 * @param folderid
	 * @return
	 */
	public List<Folder> findChildrenFolders(int folderid){
		
		Folder folder = m_entityManager.find(Folder.class, folderid);
		
		return m_entityManager.createQuery("select f from folder "
				+ "where fleft > :fleft and fright < :fright "
				+ "order by fleft asc", Folder.class)
				.setParameter("fleft", folder.getFleft())
				.setParameter("fright", folder.getFright())
				.getResultList();
	}
	
	/**
	 * Ajout d'un dossier
	 * @param folder
	 * @return
	 */
	public Folder addFolder(Folder folder, int parentFolderId){
		
		//On récupére le dossier parent
		Folder parentFolder = m_entityManager.find(Folder.class, parentFolderId);
		
		//On déplace la borne gauche, sachant qu'on s'appuie sur la borne gauche +1 du parent
		m_entityManager.createQuery("update Folder f set f.fleft = (f.fleft + 2) where f.fleft >= :fleft").setParameter("fleft", parentFolder.getFright()).executeUpdate();
		
		//On déplace la borne droite, sachant qu'on s'appuie sur la borne droite +1 du parent
		m_entityManager.createQuery("update Folder f set f.fright = (f.fright + 2) where f.fright >= :fright").setParameter("fright", parentFolder.getFright()).executeUpdate();
		
		folder.setFleft(parentFolder.getFright());
		folder.setFright(parentFolder.getFright() + 1);
		folder.setLevel(parentFolder.getLevel() + 1);
		
		m_entityManager.persist(folder);
		
		return folder;
	}
	
	/**
	 * Suppression d'un dossier
	 * @param folderId
	 * @return
	 */
	public Folder removeFolder(int folderId){
		
		Folder folder = m_entityManager.find(Folder.class, folderId);
		
		//On d�place la borne gauche, sachant qu'on s'appuie sur la borne gauche +1 du parent
		m_entityManager.createQuery("update folder set fleft = fleft - 2 where fleft >= :fleft").setParameter("fleft", folder.getFleft()).executeUpdate();
		
		//On d�place la borne droite, sachant qu'on s'appuie sur la borne droite +1 du parent
		m_entityManager.createQuery("update folder set fleft = fleft - 2 where fright >= :fright").setParameter("fright", folder.getFleft()).executeUpdate();
				
		m_entityManager.remove(folder);
		
		return folder;
	}

}
