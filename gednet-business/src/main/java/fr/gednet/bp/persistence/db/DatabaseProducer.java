package fr.gednet.bp.persistence.db;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class DatabaseProducer {

	@Produces
	@PersistenceContext(unitName = "gednetPU")
	@GednetDatabase
	public EntityManager m_entityManager;
	
}
