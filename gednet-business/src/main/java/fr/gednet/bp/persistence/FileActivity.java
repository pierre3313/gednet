package fr.gednet.bp.persistence;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;

@Entity
@Table(name="fileactivity")
public class FileActivity implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="fileactivityid")
	@SequenceGenerator(name = "job_fileactivityid_seq", sequenceName="fileactivity_fileactivityid_sequence", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "job_fileactivityid_seq")
	private int fileactivityid;
	
	@Column(name="created")
	private Timestamp created;
	
	@Column(name="fileid")
	private int fileid;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="usersid")
	private User user;
	
	@Column(name="event")
	private String event;

	public int getFileactivityid() {
		return fileactivityid;
	}
	public void setFileactivityid(int fileactivityid) {
		this.fileactivityid = fileactivityid;
	}
	public Timestamp getCreated() {
		return created;
	}
	public void setCreated(Timestamp created) {
		this.created = created;
	}
	public int getFileid() {
		return fileid;
	}
	public void setFileid(int fileid) {
		this.fileid = fileid;
	}
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	@Override
	public String toString() {
		return "FileActivity [fileactivityid=" + fileactivityid + ", created="
				+ created + ", fileid=" + fileid + ", user=" + user
				+ ", event=" + event + "]";
	}
}
