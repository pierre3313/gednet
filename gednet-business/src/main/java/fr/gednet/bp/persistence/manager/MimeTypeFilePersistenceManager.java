package fr.gednet.bp.persistence.manager;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import fr.gednet.bp.persistence.MimeTypeFile;
import fr.gednet.bp.persistence.db.GednetDatabase;

/**
 * Manager de persistance pour les objets File
 * @author epso8080
 *
 */
public class MimeTypeFilePersistenceManager {
	
	@Inject
	@GednetDatabase
	private transient EntityManager m_entityManager;
		
	/**
	 * Recherche un fichier en fonction de son id
	 * @param folderid
	 * @return
	 */
	public MimeTypeFile findMimeTypeFile(String mimetypefileid){
		
		return m_entityManager.find(MimeTypeFile.class, mimetypefileid);
		
	}

}
