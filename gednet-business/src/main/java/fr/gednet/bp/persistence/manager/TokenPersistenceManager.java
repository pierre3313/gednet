package fr.gednet.bp.persistence.manager;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import fr.gednet.bp.persistence.Token;
import fr.gednet.bp.persistence.db.GednetDatabase;

public class TokenPersistenceManager {
	
	@Inject
	@GednetDatabase
	private transient EntityManager m_entityManager;

	/**
	 * Recherche d'un token en fonction du tokenid
	 * @param tokenid
	 * @return
	 */
	public Token findTokenByTokenId(String tokenid){
		try{
			
			return m_entityManager.createQuery("select t from Token as t "
					+ "where t.tokenid = :tokenid ", Token.class)
					.setParameter("tokenid", tokenid)
					.getSingleResult();
			
		}catch(NoResultException nre){
			return null;
		}
	}

    /**
     * Recherche d'un token en fonction du usersid
     * @param usersid
     * @return
     */
    public Token findTokenByUsersId(String usersid){
        try{

            return m_entityManager.createQuery("select t from Token as t "
                    + "where t.usersid = :usersid ", Token.class)
                    .setParameter("usersid", usersid)
                    .getSingleResult();

        }catch(NoResultException nre){
            return null;
        }
    }

    /**
     * Ajout d'un Token
     * @param token
     * @return
     */
	public Token addToken(Token token){
		
		m_entityManager.persist(token);
		
		return token;
	}

    /**
     * Suppression d'un Token
     * @param tokenid
     */
	public void removeToken(String tokenid){
		
		Token token = m_entityManager.find(Token.class, tokenid);
				
		m_entityManager.remove(token);
	}

    /**
     * mise à jour d'un Token
     * @param token
     * @return
     */
	public Token updateToken(Token token){
		
		return m_entityManager.merge(token);
	}

}
