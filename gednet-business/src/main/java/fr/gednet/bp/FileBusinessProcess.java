package fr.gednet.bp;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;

import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gednet.bp.persistence.File;
import fr.gednet.bp.persistence.FileActivity;
import fr.gednet.bp.persistence.FileContent;
import fr.gednet.bp.persistence.FileTags;
import fr.gednet.bp.persistence.MimeTypeFile;
import fr.gednet.bp.persistence.manager.FileActivityPersistenceManager;
import fr.gednet.bp.persistence.manager.FileContentPersistenceManager;
import fr.gednet.bp.persistence.manager.FilePersistenceManager;
import fr.gednet.bp.persistence.manager.FileTagsPersistenceManager;
import fr.gednet.bp.persistence.manager.MimeTypeFilePersistenceManager;
import fr.gednet.util.MimeTypeUtil;

public class FileBusinessProcess {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(FileBusinessProcess.class);
	
	@Inject
    private FilePersistenceManager filePersistenceManager;
	
	@Inject
    private FileContentPersistenceManager fileContentPersistenceManager;
	
	@Inject
    private MimeTypeFilePersistenceManager mimeTypeFilePersistenceManager;
	
	@Inject
    private FileTagsPersistenceManager fileTagsPersistenceManager;
	
	@Inject
    private FileActivityPersistenceManager fileActivityPersistenceManager;
	
	/**
	 * Récupère tous les fichiers d'un dossier
	 * @param folderid
	 * @return
	 */
	public List<File> findAllFilesByFolderId(@NotNull int folderid){

		return filePersistenceManager.findAllFilesByFolderId(folderid);
	}
	
	/**
	 * Récupère un fichier en fonction de son id
	 * @param fileid
	 * @return
	 */
	public File findFile(@NotNull int fileid){

	    return filePersistenceManager.findFile(fileid);
	}
	
	/**
	 * Récupère le contenu d'un fichier en fonction de son id
	 * @param fileid
	 * @return
	 */
	public FileContent findFileContent(int fileid){

		return fileContentPersistenceManager.findFileContentByFileId(fileid);
	}
	
	/**
	 * Suppression d'un document
	 * @param fileid
	 */
	public void removeFile(int fileid){
		fileContentPersistenceManager.removeFileContent(fileid);
		fileTagsPersistenceManager.removeFileTagsByFileid(fileid);
		filePersistenceManager.removeFile(fileid);
	}
	
	/**
	 * Méthode d'ajout du fichier en base (métadata et contenu)
	 * @param fileDetail
	 * @param uploadedInputStream
	 * @param formDataBodyPart
	 * @param folderid
	 * @param length
	 * @return
	 */
	public File addFile(FormDataContentDisposition fileDetail, 
			InputStream uploadedInputStream,
			FormDataBodyPart formDataBodyPart,
			int folderid, 
			int length){
		
		//Création d'une entrée dans la table fichier
		File file = new File();
		file.setFolderid(folderid);
		String filename = null;
		try {
			filename = new String(fileDetail.getFileName().getBytes("ISO-8859-1"), "UTF-8");
		} catch (UnsupportedEncodingException e1) {
            LOGGER.error(e1.getMessage(), e1);
		}
		file.setName(filename);
		file.setSize(length);
		
		Date now = new Date();
		file.setCreated(new Timestamp(now.getTime()));
		file.setLastmodified(new Timestamp(now.getTime()));
		
		MimeTypeFile mimeTypeFile = mimeTypeFilePersistenceManager.findMimeTypeFile(formDataBodyPart.getMediaType().toString());
		if(mimeTypeFile == null)
			mimeTypeFile = mimeTypeFilePersistenceManager.findMimeTypeFile(MimeTypeUtil.identifyMimeTypeByExtension(filename));
		
		if(mimeTypeFile != null)
			file.setMimetypefile(mimeTypeFile);
		
		file.setVersionfile(1.0f);
		file.setVersionfileid(filePersistenceManager.getVersionIdFile());
		
		file = filePersistenceManager.addFile(file);
		
		//Enregistrment du flux fichier
		FileContent fileContent = new FileContent();
		fileContent.setFileid(file.getFileid());
		
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
	        int read;
	        byte[] bytes = new byte[1024];
	        
	        while ((read = uploadedInputStream.read(bytes)) != -1) {
	            out.write(bytes, 0, read);
	        }
	        
	        byte[] filecontentbyte = out.toByteArray();
	        
	        fileContent.setContent(filecontentbyte);
	        
	        out.flush();
	        out.close();
	        
	    } catch (IOException e) {
	    	LOGGER.error(e.getMessage(),e);
	    }
		
		fileContentPersistenceManager.addFileContent(fileContent);
		
		//Création d'une activité de création de document
		FileActivity fileActivity = new FileActivity();
		fileActivity.setCreated(new Timestamp(now.getTime()));
		fileActivity.setFileid(file.getFileid());
		fileActivity.setEvent("Création du dossier");
		
		fileActivityPersistenceManager.addFileActivity(fileActivity);
		
		return file;
	}
	
	/**
	 * Récupère les tags d'un fichier
	 * @param fileid
	 * @return
	 */
	public List<FileTags> findFileTags(int fileid){
		return fileTagsPersistenceManager.findFileTagsByFileid(fileid);
	}

	/**
	 * Ajout d'un tag
	 * @param fileTags
	 * @param fileid
	 * @return
	 */
	public FileTags addFileTags(FileTags fileTags, int fileid){
		
		Date now = new Date();
		fileTags.setCreated(new Timestamp(now.getTime()));
		fileTags.setFileid(fileid);
		
		return fileTagsPersistenceManager.addFileTags(fileTags);
	}

	/**
	 * Suppression d'un tag
	 * @param filetagsid
	 */
	public void removeFileTags(int filetagsid){
		
		fileTagsPersistenceManager.removeFileTags(filetagsid);
	}
	
	/**
	 * Récupère les activité d'un fichier
	 * @param fileid
	 * @return
	 */
	public List<FileActivity> findFileActivityByFileid(int fileid){
		
		return fileActivityPersistenceManager.findFileActivityByFileid(fileid);
		
	}
}
