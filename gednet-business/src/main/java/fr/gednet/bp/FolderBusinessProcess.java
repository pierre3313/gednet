package fr.gednet.bp;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import fr.gednet.bp.persistence.Folder;
import fr.gednet.bp.persistence.manager.FolderPersistenceManager;

public class FolderBusinessProcess {

	@Inject
	private FolderPersistenceManager folderPersistenceManager; 
	
	public List<Folder> findAllFolders(){
		
		return folderPersistenceManager.findAllFolders();
		
	}
	
	public List<Folder> findParentsFolders(int folderid){
		
		return folderPersistenceManager.findParentsFolders(folderid);
		
	}
	
	public Folder addFolder(Folder folder, int parentFolderId){
		
		Date now = new Date();
		folder.setCreated(new Timestamp(now.getTime()));
		
		return folderPersistenceManager.addFolder(folder, parentFolderId);
		
	}
	
}
