package fr.gednet.rs;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import fr.gednet.rs.filter.AuthenticationFilter;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

@ApplicationPath("resources")
public class ApplicationConfig extends Application
{
   @Override
   public Set<Class<?>> getClasses()
   {
      final Set<Class<?>> classes = new HashSet<>();
      
      // Déclaratif des classes exposant des ressources REST dans l'application
      classes.add(FolderResourceManager.class);
      classes.add(AuthenticationResourceManager.class);

      //Ajout filtre de sécurité
      classes.add(AuthenticationFilter.class);

      // Multipart
      classes.add(MultiPartFeature.class);
      classes.add(JacksonFeature.class);
      
      return classes;
   }
}
