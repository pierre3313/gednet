package fr.gednet.rs;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.gednet.bp.SecureBusinessProcess;
import org.omg.CORBA.UnknownUserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gednet.rs.ret.Credentials;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

@Stateless
@Path("/authentication-resource")
public class AuthenticationResourceManager {
	
private static Logger logger = LoggerFactory.getLogger(AuthenticationResourceManager.class);
	
	@Inject
	private SecureBusinessProcess secureBusinessProcess;

	@POST
	@Path("/authentication")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces()
	public Response authenticateUser(Credentials credentials){

		Response response;

		try {

			secureBusinessProcess.authenticateUsers(credentials.getEmail(), credentials.getPassword());
			response = Response.ok().header(HttpHeaders.AUTHORIZATION,"Bearer " + secureBusinessProcess.issueToken(credentials.getEmail())).build();

		} catch (InvalidKeySpecException e) {
			logger.error(e.getMessage(),e);
			response = Response.status(Response.Status.UNAUTHORIZED).build();
		} catch (NoSuchAlgorithmException e) {
			logger.error(e.getMessage(),e);
			response = Response.status(Response.Status.SERVICE_UNAVAILABLE).build();
		} catch (UnknownUserException e) {
			logger.error(e.getMessage(),e);
			response = Response.status(Response.Status.UNAUTHORIZED).build();
		}

		return response;
	}
}
