package fr.gednet.rs;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.gednet.rs.filter.security.Secured;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gednet.bp.FileBusinessProcess;
import fr.gednet.bp.FolderBusinessProcess;
import fr.gednet.bp.persistence.FileContent;
import fr.gednet.rs.ret.FileActivityResource;
import fr.gednet.rs.ret.FileResource;
import fr.gednet.rs.ret.FileTagsResource;
import fr.gednet.rs.ret.FolderResource;
import fr.gednet.rs.ret.mapping.FileActivityResourceMapping;
import fr.gednet.rs.ret.mapping.FileResourceMapping;
import fr.gednet.rs.ret.mapping.FileTagsResourceMapping;
import fr.gednet.rs.ret.mapping.FolderResourceMapping;

@Stateless
@Path("/folder-resource")
public class FolderResourceManager {
	
	private static Logger LOGGER = LoggerFactory.getLogger(FolderResourceManager.class);
	
	@Inject
	private FolderBusinessProcess folderBusinessProcess;
	
	@Inject
	private FileBusinessProcess fileBusinessProcess;
	
	@Inject 
	private FolderResourceMapping folderResourceMapping;
	
	@Inject 
	private FileResourceMapping fileResourceMapping;
	
	@Inject 
	private FileTagsResourceMapping fileTagsResourceMapping;
	
	@Inject
	private FileActivityResourceMapping fileActivityResourceMapping;
	
	@GET
	@Secured
	@Path("/folders")
	@Produces({ MediaType.APPLICATION_JSON })	
	public List<FolderResource> findAllFolders(){
		
		return folderResourceMapping.mapFolderToFolderResource(
				folderBusinessProcess.findAllFolders());
		
	}
	
	@GET
	@Path("/folders/{folderid}/parents")
	@Produces({ MediaType.APPLICATION_JSON })	
	public List<FolderResource> findParentsFolders(@PathParam("folderid") int folderid){
		
		return folderResourceMapping.mapFolderToFolderResource(
				folderBusinessProcess.findParentsFolders(folderid));
		
	}
	
	@POST
	@Path("/folders")
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public FolderResource addFolder(FolderResource folderResource){
		
		return folderResourceMapping.mapFolderToFolderResource(
				folderBusinessProcess.addFolder(
				folderResourceMapping.mapFolderResourceToFolder(folderResource), folderResource.getParentFolderid()
				)
			);
		
	}
	
	
	@GET
	@Path("/folders/{folderid}/files")
	@Produces({ MediaType.APPLICATION_JSON })	
	public List<FileResource> findAllFilesByFolderId(@PathParam("folderid") int folderid){
		
		return fileResourceMapping.mapFileToFileResource(
				fileBusinessProcess.findAllFilesByFolderId(folderid)
				);
		
	}
	
	@GET
	@Path("/folders/{folderid}/files/{fileid}")
	@Produces({ MediaType.APPLICATION_JSON })	
	public FileResource findFile(@PathParam("folderid") int folderid, @PathParam("fileid") int fileid){
		
		return fileResourceMapping.mapFileToFileResource(
				fileBusinessProcess.findFile(fileid)
				);
		
	}
	
	@GET
	@Path("/folders/{folderid}/files/{fileid}/content/{filename}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response findFileContent(@PathParam("folderid") int folderid, @PathParam("fileid") int fileid){
		
	    byte[] b;
		    
    	FileContent fileContent = fileBusinessProcess.findFileContent(fileid);
    	FileResource fileResource = fileResourceMapping.mapFileToFileResource(
				fileBusinessProcess.findFile(fileid)
				);
	    
	    b = fileContent.getContent();
	    
	    return Response.ok(b).header("content-disposition","attachment; filename=\""+fileResource.getName()+"\"").build();
		
	}
	
	@POST
	@Path("/folders/{folderid}/files")
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Consumes({MediaType.MULTIPART_FORM_DATA, MediaType.APPLICATION_FORM_URLENCODED})
	@Produces({ MediaType.APPLICATION_JSON })
	public FileResource addFile(@PathParam("folderid") int folderid,
			@HeaderParam("Content-Length") int length,
	        @FormDataParam("file") InputStream uploadedInputStream,
	        @FormDataParam("file") FormDataContentDisposition fileDetail,
	        @FormDataParam("file") FormDataBodyPart formDataBodyPart) {
				
		return fileResourceMapping.mapFileToFileResource(
				fileBusinessProcess.addFile(fileDetail, uploadedInputStream, formDataBodyPart, folderid, length)
				);
		
	}
	
	@DELETE
	@Path("/folders/{folderid}/files/{fileid}")
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Produces({ MediaType.APPLICATION_JSON })	
	public Response removeFile(@PathParam("folderid") int folderid, @PathParam("fileid") int fileid){
		
		fileBusinessProcess.removeFile(fileid);
		
		return Response.ok("ok").build();
	}
	
	@GET
	@Path("/folders/{folderid}/files/{fileid}/tags")
	@Produces({ MediaType.APPLICATION_JSON })	
	public List<FileTagsResource> findTags(@PathParam("folderid") int folderid, @PathParam("fileid") int fileid){
		
		return fileTagsResourceMapping.mapFileTagsToFileTagsResource(
				fileBusinessProcess.findFileTags(fileid));
		
	}
	
	@POST
	@Path("/folders/{folderid}/files/{fileid}/tags")
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Produces({ MediaType.APPLICATION_JSON })	
	public FileTagsResource addTags(FileTagsResource fileTagsResource, @PathParam("fileid") int fileid){
		
		return fileTagsResourceMapping.mapFileTagsToFileTagsResource(
					fileBusinessProcess.addFileTags(
						fileTagsResourceMapping.mapFileTagsResourceToFileTags(fileTagsResource), fileid
					)
				);
		
	}
	
	@DELETE
	@Path("/folders/{folderid}/files/{fileid}/tags/{filetagsid}")
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Produces({ MediaType.APPLICATION_JSON })	
	public void addTags(@PathParam("filetagsid") int filetagsid){
		
		fileBusinessProcess.removeFileTags(filetagsid);		
	}
	
	@GET
	@Path("/folders/{folderid}/files/{fileid}/activity")
	@Produces({ MediaType.APPLICATION_JSON })	
	public List<FileActivityResource> findFileActivity(@PathParam("folderid") int folderid, @PathParam("fileid") int fileid){
		
		return fileActivityResourceMapping.mapFileActivityToFileActivityResource(
				fileBusinessProcess.findFileActivityByFileid(fileid));
		
	}
}
