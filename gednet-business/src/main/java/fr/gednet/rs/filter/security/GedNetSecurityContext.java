package fr.gednet.rs.filter.security;

import javax.ws.rs.core.SecurityContext;
import java.security.Principal;

public class GedNetSecurityContext implements SecurityContext {
    private UserPrincipal userPrincipal;
    private String scheme;

    public GedNetSecurityContext(UserPrincipal userPrincipal, String scheme) {
        this.userPrincipal = userPrincipal;
        this.scheme = scheme;
    }

    @Override
    public Principal getUserPrincipal() {
        return this.userPrincipal;
    }

    @Override
    public boolean isUserInRole(String s) {
        if (userPrincipal.getRole() != null) {
            return userPrincipal.getRole().contains(s);
        }
        return false;
    }

    @Override
    public boolean isSecure() {
        return "https".equals(this.scheme);
    }

    @Override
    public String getAuthenticationScheme() {
        return SecurityContext.BASIC_AUTH;
    }
}
