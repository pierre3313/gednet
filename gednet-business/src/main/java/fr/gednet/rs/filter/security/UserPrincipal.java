package fr.gednet.rs.filter.security;

import java.security.Principal;
import java.util.List;

/**
 * Class pour le contexte de securité
 */
public class UserPrincipal implements Principal {

    private String id;
    private String firstName;
    private String lastName;
    private String login;
    private String password;
    private List<String> role;

    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return this.id;
    }
    public String getFirstName() {
        return this.firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return this.lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public List<String> getRole() {
        return role;
    }
    public void setRole(List<String> role) {
        this.role = role;
    }

    @Override
    public String getName() {
        return this.firstName + " " + this.lastName;
    }
}
