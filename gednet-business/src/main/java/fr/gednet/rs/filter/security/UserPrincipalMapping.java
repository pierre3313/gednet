package fr.gednet.rs.filter.security;

import fr.gednet.bp.persistence.Groups;
import fr.gednet.bp.persistence.User;

import java.util.ArrayList;
import java.util.List;

public class UserPrincipalMapping {

    public UserPrincipal mapUserToUserPrincipal(User user){

        UserPrincipal userPrincipal = new UserPrincipal();

        userPrincipal.setId(user.getUsersid());
        userPrincipal.setLogin(user.getUsersid());
        userPrincipal.setFirstName(user.getFirstname());
        userPrincipal.setLastName(user.getName());
        userPrincipal.setPassword(user.getPassword());

        List<String> roles = new ArrayList<>();
        for(Groups group : user.getGroups()){
            roles.add(group.getGroupType().getGrouptypeid());
        }

        userPrincipal.setRole(roles);

        return userPrincipal;
    }
}
