package fr.gednet.rs.filter;

import fr.gednet.bp.SecureBusinessProcess;
import fr.gednet.bp.persistence.User;
import fr.gednet.rs.filter.security.GedNetSecurityContext;
import fr.gednet.rs.filter.security.Secured;
import fr.gednet.rs.filter.security.UserPrincipal;
import fr.gednet.rs.filter.security.UserPrincipalMapping;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

@Secured
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {

    @Inject
    SecureBusinessProcess secureBusinessProcess;

    @Inject
    UserPrincipalMapping userPrincipalMapping;

    @Override
    public void filter(ContainerRequestContext requestContext) {

        // Get the HTTP Authorization header from the request
        String authorizationHeader =
                requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);

        // Check if the HTTP Authorization header is present and formatted correctly
        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
            throw new NotAuthorizedException("Authorization header doit etre fourni");
        }

        // Extract the token from the HTTP Authorization header
        String token = authorizationHeader.substring("Bearer".length() + 1).trim();

        try {

            // Valide the token
            boolean isTokenValid = secureBusinessProcess.validateToken(token);
            if(!isTokenValid)
                throw new NotAuthorizedException("Token non valide");

            //création du contexte de sécurité personnalisé
            User user = secureBusinessProcess.getUserAuthenticated(token);
            UserPrincipal userPrincipal = userPrincipalMapping.mapUserToUserPrincipal(user);

            String scheme = requestContext.getUriInfo().getRequestUri().getScheme();
            GedNetSecurityContext gedNetSecurityContext = new GedNetSecurityContext(userPrincipal, scheme);
            requestContext.setSecurityContext(gedNetSecurityContext);


        } catch (Exception e) {
            requestContext.abortWith(
                    Response.status(Response.Status.UNAUTHORIZED).build());
        }
    }


}
