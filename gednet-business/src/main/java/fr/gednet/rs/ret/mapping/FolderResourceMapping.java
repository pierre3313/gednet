package fr.gednet.rs.ret.mapping;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import fr.gednet.bp.persistence.Folder;
import fr.gednet.rs.ret.FolderResource;

public class FolderResourceMapping {
		
	public FolderResource mapFolderToFolderResource(Folder folder){
		
		FolderResource folderResource = new FolderResource();
		
		folderResource.setFolderid(folder.getFolderid());
		folderResource.setFleft(folder.getFleft());
		folderResource.setLevel(folder.getLevel());
		folderResource.setName(folder.getName());
		folderResource.setFright(folder.getFright());
		if(folder.getCreated() != null)
			folderResource.setCreated(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(folder.getCreated()));
		
		return folderResource;
	}
	
	public Folder mapFolderResourceToFolder(FolderResource folderResource){
		
		Folder folder = new Folder();
		
		folder.setFolderid(folderResource.getFolderid());
		folder.setFleft(folderResource.getFleft());
		folder.setLevel(folderResource.getLevel());
		folder.setName(folderResource.getName());
		folder.setFright(folderResource.getFright());
		
		return folder;
		
	}
	
	public List<FolderResource> mapFolderToFolderResource(List<Folder> lFolders){
		
		List<FolderResource> lFolderResources = new ArrayList<FolderResource>();
		for(Folder folder : lFolders){
			lFolderResources.add(mapFolderToFolderResource(folder));
		}
		
		return lFolderResources;
	}
	
	public List<Folder> mapFolderResourceToFolder(List<FolderResource> lFolderResources){
		
		List<Folder> lFolders = new ArrayList<Folder>();
		for(FolderResource folderResource : lFolderResources){
			lFolders.add(mapFolderResourceToFolder(folderResource));
		}
		
		return lFolders;
	}

}
