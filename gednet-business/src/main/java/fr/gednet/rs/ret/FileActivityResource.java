package fr.gednet.rs.ret;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class FileActivityResource {
	
	private int fileactivityid;
	private  String created;
	private int fileid;
	private String event;
	private UserResource userResource;
	
	public int getFileactivityid() {
		return fileactivityid;
	}
	public void setFileactivityid(int fileactivityid) {
		this.fileactivityid = fileactivityid;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public int getFileid() {
		return fileid;
	}
	public void setFileid(int fileid) {
		this.fileid = fileid;
	}
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}	
	public UserResource getUserResource() {
		return userResource;
	}
	public void setUserResource(UserResource userResource) {
		this.userResource = userResource;
	}
	
	@Override
	public String toString() {
		return "FileActivityResource [fileactivityid=" + fileactivityid
				+ ", created=" + created + ", fileid=" + fileid + ", event="
				+ event + "]";
	}	
}
