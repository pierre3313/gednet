package fr.gednet.rs.ret;

import java.sql.Timestamp;

public class FileTagsResource {
	
	private int filetagsid;
	private Timestamp created;
	private int fileid;
	private String tag;
	
	public int getFiletagsid() {
		return filetagsid;
	}
	public void setFiletagsid(int filetagsid) {
		this.filetagsid = filetagsid;
	}
	public Timestamp getCreated() {
		return created;
	}
	public void setCreated(Timestamp created) {
		this.created = created;
	}
	public int getFileid() {
		return fileid;
	}
	public void setFileid(int fileid) {
		this.fileid = fileid;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	
	@Override
	public String toString() {
		return "FileTagsResource [filetagsid=" + filetagsid + ", created="
				+ created + ", fileid=" + fileid + ", tag=" + tag + "]";
	}
}
