package fr.gednet.rs.ret.mapping;

import java.util.ArrayList;
import java.util.List;

import fr.gednet.bp.persistence.FileTags;
import fr.gednet.rs.ret.FileTagsResource;

public class FileTagsResourceMapping {
	
	public FileTagsResource mapFileTagsToFileTagsResource(FileTags fileTags){		
		FileTagsResource fileTagsResource = new FileTagsResource();
		
		fileTagsResource.setFiletagsid(fileTags.getFiletagsid());
		fileTagsResource.setCreated(fileTags.getCreated());
		fileTagsResource.setTag(fileTags.getTag());
		fileTagsResource.setFileid(fileTags.getFileid());
		
		return fileTagsResource;
	}
	
	public FileTags mapFileTagsResourceToFileTags(FileTagsResource fileTagsResource){		
		FileTags fileTag = new FileTags();
		
		fileTag.setFiletagsid(fileTagsResource.getFiletagsid());
		fileTag.setCreated(fileTagsResource.getCreated());
		fileTag.setTag(fileTagsResource.getTag());
		fileTag.setFileid(fileTagsResource.getFileid());
		
		return fileTag;		
	}
	
	public List<FileTagsResource> mapFileTagsToFileTagsResource(List<FileTags> lFileTags){
		
		List<FileTagsResource> lFileTagsResources = new ArrayList<FileTagsResource>();
		for(FileTags fileTags : lFileTags){
			lFileTagsResources.add(mapFileTagsToFileTagsResource(fileTags));
		}
		
		return lFileTagsResources;
	}
	
	public List<FileTags> mapFileTagsResourceToFileTags(List<FileTagsResource> lFileTagsResources){
		
		List<FileTags> lFileTags = new ArrayList<FileTags>();
		for(FileTagsResource fileResource : lFileTagsResources){
			lFileTags.add(mapFileTagsResourceToFileTags(fileResource));
		}
		
		return lFileTags;
	}

}
