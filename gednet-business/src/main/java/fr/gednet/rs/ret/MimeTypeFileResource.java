package fr.gednet.rs.ret;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MimeTypeFileResource {
	
	private String mimetypefileid;
	private String mimetypefilename;
	
	public String getMimetypefileid() {
		return mimetypefileid;
	}
	public void setMimetypefileid(String mimetypefileid) {
		this.mimetypefileid = mimetypefileid;
	}
	public String getMimetypefilename() {
		return mimetypefilename;
	}
	public void setMimetypefilename(String mimetypefilename) {
		this.mimetypefilename = mimetypefilename;
	}
	
	@Override
	public String toString() {
		return "MimeTypeFileResource [mimetypefileid=" + mimetypefileid
				+ ", mimetypefilename=" + mimetypefilename + "]";
	}
}
