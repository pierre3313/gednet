package fr.gednet.rs.ret.mapping;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import fr.gednet.bp.persistence.File;
import fr.gednet.bp.persistence.MimeTypeFile;
import fr.gednet.rs.ret.FileResource;
import fr.gednet.rs.ret.MimeTypeFileResource;

public class FileResourceMapping {
	
	public FileResource mapFileToFileResource(File file){
		
		FileResource fileResource = new FileResource();
		MimeTypeFileResource mimeTypeFileResource = new MimeTypeFileResource();
		
		if(file.getMimetypefile() != null){
			mimeTypeFileResource.setMimetypefileid(file.getMimetypefile().getMimetypefileid());
			mimeTypeFileResource.setMimetypefilename(file.getMimetypefile().getMimetypefilename());
		}
		
		fileResource.setFileid(file.getFileid());
		fileResource.setVersionfileid(file.getVersionfileid());
		
		NumberFormat df = DecimalFormat.getInstance();
		df.setRoundingMode(RoundingMode.FLOOR);
		df.setMinimumFractionDigits(2);
        df.setMaximumFractionDigits(2);
        
		fileResource.setVersionfile(df.format(new Double(file.getVersionfile())).replaceAll(",", "."));
		fileResource.setFolderid(file.getFolderid());
		fileResource.setName(file.getName());
		fileResource.setSize(file.getSize());
		if(file.getCreated() != null)
			fileResource.setCreated(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(file.getCreated()));
		if(file.getLastmodified() != null)
			fileResource.setLastmodified(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(file.getLastmodified()));
		fileResource.setMimeTypeFileResource(mimeTypeFileResource);
		
		return fileResource;
	}
	
	public File mapFileResourceToFile(FileResource fileResource){
		
		File file = new File();
		MimeTypeFile mimeTypeFile = new MimeTypeFile();
		
		if(fileResource.getMimeTypeFileResource() != null){
			mimeTypeFile.setMimetypefileid(fileResource.getMimeTypeFileResource().getMimetypefileid());
			mimeTypeFile.setMimetypefilename(fileResource.getMimeTypeFileResource().getMimetypefilename());
		}
		
		file.setFileid(fileResource.getFileid());
		file.setFolderid(fileResource.getFolderid());
		file.setName(fileResource.getName());
		file.setSize(fileResource.getSize());
		file.setMimetypefile(mimeTypeFile);
		
		return file;
	}
	
	public List<FileResource> mapFileToFileResource(List<File> lFiles){
		
		List<FileResource> lFileResources = new ArrayList<FileResource>();
		for(File file : lFiles){
			lFileResources.add(mapFileToFileResource(file));
		}
		
		return lFileResources;
	}
	
	public List<File> mapFileResourceToFile(List<FileResource> lFileResources){
		
		List<File> lFiles = new ArrayList<File>();
		for(FileResource fileResource : lFileResources){
			lFiles.add(mapFileResourceToFile(fileResource));
		}
		
		return lFiles;
	}

}
