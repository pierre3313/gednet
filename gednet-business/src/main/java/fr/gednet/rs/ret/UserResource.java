package fr.gednet.rs.ret;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UserResource {
	
	private String usersid;
	private String name;
	private String firstname;
	private int groupsid;

	public String getUsersid() {
		return usersid;
	}
	public void setUsersid(String usersid) {
		this.usersid = usersid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public int getGroupsid() {
		return groupsid;
	}
	public void setGroupsid(int groupsid) {
		this.groupsid = groupsid;
	}

	@Override
	public String toString() {
		return "UserResource{" +
				"usersid=" + usersid +
				", name='" + name + '\'' +
				", firstname='" + firstname + '\'' +
				", groupsid=" + groupsid +
				'}';
	}
}
