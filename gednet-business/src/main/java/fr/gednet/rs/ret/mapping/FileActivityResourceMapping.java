package fr.gednet.rs.ret.mapping;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import fr.gednet.bp.persistence.FileActivity;
import fr.gednet.rs.ret.FileActivityResource;
import fr.gednet.rs.ret.UserResource;

public class FileActivityResourceMapping {
	
	@Inject
	UserResourceMapping userResourceMapping;
	
	public FileActivityResource mapFileActivityToFileActivityResource(FileActivity fileActivity){
		
		FileActivityResource fileActivityResource =  new FileActivityResource();
		UserResource userResource;

		fileActivityResource.setFileactivityid(fileActivity.getFileactivityid());
		fileActivityResource.setFileid(fileActivity.getFileid());
		fileActivityResource.setCreated(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(fileActivity.getCreated()));
		fileActivityResource.setEvent(fileActivity.getEvent());

		if(fileActivity.getUser() != null){
			userResource = userResourceMapping.mapUserToUserResource(fileActivity.getUser());
			fileActivityResource.setUserResource(userResource);
		}
		
		return fileActivityResource;
	}
	
	public List<FileActivityResource> mapFileActivityToFileActivityResource(List<FileActivity> lFileActivitys){
		
		List<FileActivityResource> lFileActivityResources = new ArrayList<FileActivityResource>();

		for(FileActivity fileActivity : lFileActivitys){
			lFileActivityResources.add(mapFileActivityToFileActivityResource(fileActivity));
		}
		
		return lFileActivityResources;
	}
	
}
