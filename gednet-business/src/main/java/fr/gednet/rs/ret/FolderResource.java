package fr.gednet.rs.ret;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class FolderResource {
	
	private int folderid;
	private int parentFolderid;
	private int level;	
	private int fleft;
	private int fright;
	private String name;
	private String created;
	
	public int getFolderid() {
		return folderid;
	}
	public void setFolderid(int folderid) {
		this.folderid = folderid;
	}
	public int getParentFolderid() {
		return parentFolderid;
	}
	public void setParentFolderid(int parentFolderid) {
		this.parentFolderid = parentFolderid;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public int getFleft() {
		return fleft;
	}
	public void setFleft(int fleft) {
		this.fleft = fleft;
	}
	public int getFright() {
		return fright;
	}
	public void setFright(int fright) {
		this.fright = fright;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}	
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	
	@Override
	public String toString() {
		return "FolderResource [folderid=" + folderid + ", parentFolderid="
				+ parentFolderid + ", level=" + level + ", fleft=" + fleft
				+ ", fright=" + fright + ", name=" + name + "]";
	}
}
