package fr.gednet.rs.ret.mapping;

import fr.gednet.bp.persistence.User;
import fr.gednet.rs.ret.UserResource;

public class UserResourceMapping {
	
	public UserResource mapUserToUserResource(User user){
		
		UserResource userResource = new UserResource();
		
		userResource.setUsersid(user.getUsersid());
		userResource.setFirstname(user.getFirstname());
		userResource.setName(user.getName());
		userResource.setUsersid(user.getUsersid());
		
		return userResource;
		
	}
	
	public User mapUserResourceToUser(UserResource userResource){
		
		User user = new User();
		
		user.setUsersid(userResource.getUsersid());
		user.setFirstname(userResource.getFirstname());
		user.setName(userResource.getName());
		user.setUsersid(userResource.getUsersid());
		
		return user;
		
	}

}
