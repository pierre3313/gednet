package fr.gednet.rs.ret;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class FileResource {
	
	private int fileid;
	private int versionfileid;
	private String versionfile;
	private String name;
	private long size;
	private String created;
	private String lastmodified;
	
	private MimeTypeFileResource mimeTypeFileResource;
	private int folderid;
	
	public int getFileid() {
		return fileid;
	}
	public void setFileid(int fileid) {
		this.fileid = fileid;
	}
	public int getVersionfileid() {
		return versionfileid;
	}
	public void setVersionfileid(int versionfileid) {
		this.versionfileid = versionfileid;
	}
	public String getVersionfile() {
		return versionfile;
	}
	public void setVersionfile(String versionfile) {
		this.versionfile = versionfile;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getSize() {
		return size;
	}
	public void setSize(long size) {
		this.size = size;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getLastmodified() {
		return lastmodified;
	}
	public void setLastmodified(String lastmodified) {
		this.lastmodified = lastmodified;
	}
	public MimeTypeFileResource getMimeTypeFileResource() {
		return mimeTypeFileResource;
	}
	public void setMimeTypeFileResource(MimeTypeFileResource mimeTypeFileResource) {
		this.mimeTypeFileResource = mimeTypeFileResource;
	}
	public int getFolderid() {
		return folderid;
	}
	public void setFolderid(int folderid) {
		this.folderid = folderid;
	}
	
	@Override
	public String toString() {
		return "FileResource [fileid=" + fileid + ", name=" + name + ", size="
				+ size + ", mimeTypeFileResource=" + mimeTypeFileResource
				+ ", folderid=" + folderid + "]";
	}
}
