package fr.gednet.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class SecurityUtilTest {

    @Test
    public void testGenerateStrongPasswordHash() throws InvalidKeySpecException, NoSuchAlgorithmException {

        String hashPassword = SecurityUtil.generateStrongPasswordHash("123456");
        assertNotNull(hashPassword);
    }

    @Test
    public void testValidatePassword() throws InvalidKeySpecException, NoSuchAlgorithmException {

        String hashPassword = SecurityUtil.generateStrongPasswordHash("123456");

        boolean passwordValid = SecurityUtil.validatePassword("123456", hashPassword);
        assertEquals(true, passwordValid);

        passwordValid = SecurityUtil.validatePassword("1235467", hashPassword);
        assertEquals(false, passwordValid);
    }

}
