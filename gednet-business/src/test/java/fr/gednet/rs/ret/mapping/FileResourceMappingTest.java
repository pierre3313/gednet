package fr.gednet.rs.ret.mapping;

import fr.gednet.bp.persistence.File;
import fr.gednet.rs.ret.FileResource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class FileResourceMappingTest {

    @InjectMocks
    private FileResourceMapping fileResourceMapping;

    @Test(expected = NullPointerException.class)
    public void testMapFileToFileResource_withNullParam(){

        File file = null;
        assertNull(fileResourceMapping.mapFileToFileResource(file));

        List<File> lFiles = null;
        assertNull(fileResourceMapping.mapFileToFileResource(lFiles));
    }

    @Test
    public void testMapFileToFileResource_withValidParam(){

        FileResource fileResource = fileResourceMapping.mapFileToFileResource(ResourceUtils.createFile(0));

        assertNotNull(fileResource);
        assertNotNull(fileResource.getMimeTypeFileResource());

        assertEquals(0, fileResource.getFileid());
        assertEquals(0, fileResource.getFolderid());

    }

    @Test(expected = NullPointerException.class)
    public void testMapFileResourceToFile_withNullParam(){
        FileResource fileResource = null;
        assertNull(fileResourceMapping.mapFileResourceToFile(fileResource));

        List<FileResource> lFileResources = null;
        assertNull(fileResourceMapping.mapFileResourceToFile(lFileResources));
    }

    @Test
    public void testMapFileResourceToFile_withValidParam(){

        File file = fileResourceMapping.mapFileResourceToFile(ResourceUtils.createFileResource(0));

        assertNotNull(file);
        assertNotNull(file.getMimetypefile());

        assertEquals(0, file.getFileid());
        assertEquals(0, file.getFolderid());

    }

    @Test
    public void testMapFileResourceToFile_withValidListParam(){

        List<File> lFiles = new ArrayList<>();
        for(int i = 0; i < 10; i++){
            lFiles.add(ResourceUtils.createFile(i));
        }

        List<FileResource> lFileResources = fileResourceMapping.mapFileToFileResource(lFiles);

        assertNotNull(lFileResources);
        assertEquals(10, lFileResources.size());
    }

    @Test
    public void testMapFileToFileResource_withValidListParam(){

        List<FileResource> lFileResources = new ArrayList<>();
        for(int i = 0; i < 10; i++){
            lFileResources.add(ResourceUtils.createFileResource(i));
        }

        List<File> lFile = fileResourceMapping.mapFileResourceToFile(lFileResources);

        assertNotNull(lFile);
        assertEquals(10, lFile.size());
    }

}
