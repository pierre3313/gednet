package fr.gednet.rs.ret.mapping;

import fr.gednet.bp.persistence.FileTags;
import fr.gednet.rs.ret.FileTagsResource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class FileTagsResourceMappingTest {

    @InjectMocks
    FileTagsResourceMapping fileTagsResourceMapping;

    @Test
    public void mapFileTagsToFileTagsResource_withValidParam(){

        FileTagsResource fileTagsResource = fileTagsResourceMapping.mapFileTagsToFileTagsResource(ResourceUtils.createFileTags(0));

        assertNotNull(fileTagsResource);

        assertEquals(0, fileTagsResource.getFiletagsid());
        assertEquals(0, fileTagsResource.getFileid());

    }

    @Test
    public void mapFileTagsResourceToFileTags_withValidParam(){

        FileTags fileTags = fileTagsResourceMapping.mapFileTagsResourceToFileTags(ResourceUtils.createFileTagsResource(0));

        assertNotNull(fileTags);

        assertEquals(0, fileTags.getFiletagsid());
        assertEquals(0, fileTags.getFileid());

    }

    @Test
    public void mapFileTagsToFileTagsResource_withValidListParam(){

        List<FileTags> fileTags = new ArrayList<>();
        for(int i = 0; i < 10; i++){
            fileTags.add(ResourceUtils.createFileTags(i));
        }

        List<FileTagsResource> fileTagsResources = fileTagsResourceMapping.mapFileTagsToFileTagsResource(fileTags);

        assertNotNull(fileTagsResources);

        assertEquals(10, fileTagsResources.size());
    }

    @Test
    public void mapFileTagsResourceToFileTags_withValidListParam(){

        List<FileTagsResource> fileTagsResources = new ArrayList<>();
        for(int i = 0; i < 10; i++){
            fileTagsResources.add(ResourceUtils.createFileTagsResource(i));
        }

        List<FileTags> fileTags = fileTagsResourceMapping.mapFileTagsResourceToFileTags(fileTagsResources);

        assertNotNull(fileTags);

        assertEquals(10, fileTags.size());

    }
}
