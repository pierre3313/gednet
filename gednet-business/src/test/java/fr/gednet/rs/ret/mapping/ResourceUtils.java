package fr.gednet.rs.ret.mapping;

import fr.gednet.bp.SecureBusinessProcess;
import fr.gednet.bp.persistence.*;
import fr.gednet.rs.ret.FileResource;
import fr.gednet.rs.ret.FileTagsResource;
import fr.gednet.rs.ret.MimeTypeFileResource;
import fr.gednet.rs.ret.UserResource;
import fr.gednet.util.SecurityUtil;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.Date;

public class ResourceUtils {

	/**
	 * Crée un bean FileActivity pour les tests
	 * @param idTest
	 * @return
	 */
	public static FileActivity createFileActivity(int idTest){
		
		FileActivity fileActivity = new FileActivity();
		User user = createUser();
		
		Date now = new Date();
		fileActivity.setCreated(new Timestamp(now.getTime()));
		fileActivity.setEvent("Evenemment TEST " + idTest);
		fileActivity.setFileactivityid(idTest);
		fileActivity.setFileid(idTest);
		fileActivity.setUser(user);
		
		return fileActivity;
		
	}

    /**
     * Crée un bean User pour les tests
     * @return
     */
	public static User createUser(){
		
		User user = new User();
		
		user.setUsersid("test@mail.fr");
		user.setFirstname("TEST");
		user.setName("TEST");
		user.setPassword("TEST");
		
		return user;
	}

    /**
     * Crée un bean User Resourcepour les tests
     * @return
     */
	public static UserResource createUserResource(){

		UserResource userResource = new UserResource();

		userResource.setUsersid("test@mail.fr");
		userResource.setFirstname("TEST");
		userResource.setName("TEST");

		return userResource;
	}

	/**
	 * Crée un bean File pour les tests
	 * @param idTest
	 * @return
	 */
	public static File createFile(int idTest){

		File file = new File();

		file.setFileid(idTest);
		file.setFolderid(idTest);

		MimeTypeFile mimeTypeFile = new MimeTypeFile();
		mimeTypeFile.setMimetypefileid("MIME TEST");
		mimeTypeFile.setMimetypefilename("TEST");

		file.setMimetypefile(mimeTypeFile);
		file.setName("FILE TEST");
		file.setSize(10L);
		Date now = new Date();
		Timestamp timestamp = new Timestamp(now.getTime());
		file.setCreated(timestamp);
		file.setLastmodified(timestamp);
		file.setVersionfile(new Float(1.0));

		return file;
	}

	/**
	 * Créé un bean FileResource pour les tests
	 * @param idTest
	 * @return
	 */
	public static FileResource createFileResource(int idTest){

		FileResource fileResource = new FileResource();

		fileResource.setFileid(idTest);
		fileResource.setFolderid(idTest);

		MimeTypeFileResource mimeTypeFileResource = new MimeTypeFileResource();
		mimeTypeFileResource.setMimetypefileid("MIME TEST");
		mimeTypeFileResource.setMimetypefilename("TEST");

		fileResource.setMimeTypeFileResource(mimeTypeFileResource);
		fileResource.setName("FILE TEST");
		fileResource.setSize(10L);
		fileResource.setCreated("2018-01-01");
		fileResource.setLastmodified("2018-01-01");
		fileResource.setVersionfile("1.0");

		return fileResource;
	}

    /**
     * Créé un bean FileTag pour les tests
     * @param idTest
     * @return
     */
	public static FileTags createFileTags(int idTest){

	    FileTags fileTags = new FileTags();

	    fileTags.setFiletagsid(idTest);
	    fileTags.setFileid(idTest);
        Date now = new Date();
        Timestamp timestamp = new Timestamp(now.getTime());
	    fileTags.setCreated(timestamp);
	    fileTags.setTag("TAG TEST");

	    return fileTags;
    }

    /**
     * Créé un bean FileTagResource pour les tests
     * @param idTest
     * @return
     */
    public static FileTagsResource createFileTagsResource(int idTest){

	    FileTagsResource fileTagsResource = new FileTagsResource();

	    fileTagsResource.setFiletagsid(idTest);
	    fileTagsResource.setFileid(idTest);
        Date now = new Date();
        Timestamp timestamp = new Timestamp(now.getTime());
	    fileTagsResource.setCreated(timestamp);
	    fileTagsResource.setTag("TAG TEST");

	    return fileTagsResource;

    }

    public static FileContent createFileContentResource(int idTest){

        FileContent fileContent = new FileContent();

        fileContent.setContent(new byte[]{0});
        fileContent.setFileid(idTest);
        fileContent.setFilecontentid(idTest);

        return fileContent;
    }

    public static Token createToken() throws NoSuchAlgorithmException {

        //Génération de la clé secrete pour chriffrement jeton
        String salt = SecurityUtil.toHex(SecurityUtil.getSalt());

        String tokenChain = SecurityUtil.buildJwtBuilder(
                salt,
                "test@mail.fr",
                "TEST",
                "test@mail.fr",
                SecureBusinessProcess.TOKEN_EXPIRATION
        ).compact();

        String saltRefresh = SecurityUtil.toHex(SecurityUtil.getSalt());

        String refreshTokenChain = SecurityUtil.buildJwtBuilder(
                saltRefresh,
                "test@mail.fr",
                "TEST",
                "test@mail.fr",
                SecureBusinessProcess.REFRESH_TOKEN_EXPIRATION).compact();

    	Token token = new Token();

    	token.setUsersid("test@mail.fr");
		token.setTokenid(tokenChain);
    	token.setRefreshtoken(refreshTokenChain);
		token.setSalt(salt);

		return token;
	}

}
