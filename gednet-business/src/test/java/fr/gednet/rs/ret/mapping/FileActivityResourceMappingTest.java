package fr.gednet.rs.ret.mapping;

import fr.gednet.bp.persistence.FileActivity;
import fr.gednet.bp.persistence.User;
import fr.gednet.rs.ret.FileActivityResource;
import fr.gednet.rs.ret.UserResource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class FileActivityResourceMappingTest {
	
	@InjectMocks
    FileActivityResourceMapping fileActivityResourceMapping;

	@Mock
    UserResourceMapping userResourceMapping;
		
	@Test(expected = NullPointerException.class)
	public void testMapFileActivityToFileActivityResource_withNullParam(){
		FileActivity fileActivity = null;
		assertNull(fileActivityResourceMapping.mapFileActivityToFileActivityResource(fileActivity));
		
		List<FileActivity> lFileActivitys = null;
		assertNull(fileActivityResourceMapping.mapFileActivityToFileActivityResource(lFileActivitys));
	}
	
	@Test
	public void testMapFileActivityToFileActivityResource_withValidParam(){
		
		Mockito.when(userResourceMapping.mapUserToUserResource(Mockito.any(User.class)))
		.thenReturn(ResourceUtils.createUserResource());
		
		FileActivityResource fileActivityResource = fileActivityResourceMapping.
				mapFileActivityToFileActivityResource(ResourceUtils.createFileActivity(0));
		assertNotNull(fileActivityResource);
		assertNotNull(fileActivityResource.getUserResource());
		
		assertEquals(0, fileActivityResource.getFileid());
		assertEquals(0,fileActivityResource.getFileactivityid());
		assertEquals("test@mail.fr", fileActivityResource.getUserResource().getUsersid());
		
	}

}

