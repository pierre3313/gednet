package fr.gednet.bp;

import java.util.ArrayList;
import java.util.List;

import fr.gednet.bp.persistence.File;
import fr.gednet.bp.persistence.FileContent;
import fr.gednet.rs.ret.mapping.ResourceUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import fr.gednet.bp.persistence.manager.FileActivityPersistenceManager;
import fr.gednet.bp.persistence.manager.FileContentPersistenceManager;
import fr.gednet.bp.persistence.manager.FilePersistenceManager;
import fr.gednet.bp.persistence.manager.FileTagsPersistenceManager;
import fr.gednet.bp.persistence.manager.MimeTypeFilePersistenceManager;

import static org.mockito.Matchers.anyInt;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class FileBusinnessProcessTest {

	@InjectMocks
    private FileBusinessProcess fileBusinessProcess;

	@Mock
    private FilePersistenceManager filePersistenceManager;
	
	@Mock
    private FileContentPersistenceManager fileContentPersistenceManager;
	
	@InjectMocks
    private MimeTypeFilePersistenceManager mimeTypeFilePersistenceManager;
	
	@InjectMocks
    private FileTagsPersistenceManager fileTagsPersistenceManager;
	
	@InjectMocks
    private FileActivityPersistenceManager fileActivityPersistenceManager;
	
	@Test
	public void testFindAllFilesByFolderId(){

		List<File> lFiles = new ArrayList<>();
		for(int i = 0; i < 10; i++){
			lFiles.add(ResourceUtils.createFile(i));
		}

		Mockito.when(filePersistenceManager.findAllFilesByFolderId(anyInt()))
				.thenReturn(lFiles);


        List<File> files = fileBusinessProcess.findAllFilesByFolderId(1);

        assertNotNull(files);
        assertEquals(10, files.size());
	}

    @Test
    public void testFindFile(){

        Mockito.when(filePersistenceManager.findFile(anyInt()))
                .thenReturn(ResourceUtils.createFile(0));

        File file = fileBusinessProcess.findFile(0);

        assertNotNull(file);
        assertEquals(0, file.getFileid());
    }

    @Test
    public void testFindFileContent(){

        Mockito.when(fileContentPersistenceManager.findFileContentByFileId(anyInt()))
                .thenReturn(ResourceUtils.createFileContentResource(0));

        FileContent fileContent = fileBusinessProcess.findFileContent(0);

        assertNotNull(fileContent);
        assertEquals(0, fileContent.getFilecontentid());

    }
}
