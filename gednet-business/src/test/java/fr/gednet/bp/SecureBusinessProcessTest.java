package fr.gednet.bp;

import fr.gednet.bp.persistence.Token;
import fr.gednet.bp.persistence.manager.TokenPersistenceManager;
import fr.gednet.bp.persistence.manager.UsersPersistenceManager;
import fr.gednet.rs.ret.mapping.ResourceUtils;
import fr.gednet.util.SecurityUtil;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.omg.CORBA.UnknownUserException;

import java.security.NoSuchAlgorithmException;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;


@RunWith(MockitoJUnitRunner.class)
public class SecureBusinessProcessTest {

    @InjectMocks
    SecureBusinessProcess secureBusinessProcess;

    @Mock
    UsersPersistenceManager usersPersistenceManager;

    @Mock
    TokenPersistenceManager tokenPersistenceManager;

    @Test
    public void testIssueToken_withValidParam() throws NoSuchAlgorithmException, UnknownUserException {

        Mockito.when(usersPersistenceManager.findUsersByUsersid(anyString()))
                .thenReturn(ResourceUtils.createUser());

        Mockito.when(tokenPersistenceManager.findTokenByUsersId(anyString()))
                .thenReturn(null);

        Mockito.when(tokenPersistenceManager.addToken(Mockito.any(Token.class)))
                .thenReturn(null);

        String token = secureBusinessProcess.issueToken("test@mail.fr");

        assertNotNull(token);
    }

    @Test
    public void testValidateToken_withValidParam() throws UnknownUserException, NoSuchAlgorithmException {

        Token token = ResourceUtils.createToken();

        Mockito.when(tokenPersistenceManager.findTokenByTokenId(anyString()))
                .thenReturn(token);

        boolean isValideToken = secureBusinessProcess.validateToken(token.getTokenid());
        assertEquals(true, isValideToken);
    }

    @Test(expected = MalformedJwtException.class)
    public void testValidateToken_withInvalidParam() throws UnknownUserException, NoSuchAlgorithmException {

        Token token = ResourceUtils.createToken();

        Mockito.when(tokenPersistenceManager.findTokenByTokenId(anyString()))
                .thenReturn(token);

        boolean isValideToken = secureBusinessProcess.validateToken("hash_fault");
        assertEquals(false, isValideToken);
    }

}
