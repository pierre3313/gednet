/**
 * 
 */
$(document).ready(
	function(){
		authenticateUser();
	}
);

function authenticateUser(){
	
	$("#login").focus();
	
	//Vérification présence token
	var token = $.cookie("gednet_token");
	if(token !== null){
		var credentials = new Object();
		credentials.hashsha1 = token;
		$.ajax({
			url : "/gednet-business/resources/authentication-resource/authentication/",
			type : "POST",
			dataType: "json",
			data: JSON.stringify(credentials),
			contentType: 'application/json',
	        success: function(token){
	        	$.cookie("gednet_token",token);
	        	window.location.href="gednet.html";
	        },
	        error: function(){
	        	
	        }
		});
	}
	
	$("#authenticate").click(
		function(){
			var credentials = new Object();
			credentials.email = $("#login").val();
			credentials.password = $("#password").val();
			$.ajax({
				url : "/gednet-business/resources/authentication-resource/authentication/",
				type : "POST",
				data: JSON.stringify(credentials),
				contentType: 'application/json',
		        success: function(data, textStatus, request){
		            var token = request.getResponseHeader("Authorization");
		        	$.cookie("gednet_token",token);
		        	window.location.href="gednet.html";
		        },
		        error: function(){
		        	$("div.welcome-error-authentication-main-content").show();
		        }
			});
		}
	);
	
	$("input.welcome-input-authentication-main-content").keyup(
		function(e){
			var code = e.keyCode || e.which;				
			 if(code == 13) {
				 $("#authenticate").click();
			 }
		}
	);
}

