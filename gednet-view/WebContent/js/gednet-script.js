jQuery.fn.exists = function(){return this.length>0;};

jQuery.fn.tagName = function() {return this.prop("tagName");};

function limitLength(texttolimit, lengthmax) {	
	if (typeof texttolimit === "string"){
		if(texttolimit.length > lengthmax){
			texttolimit = texttolimit.substring(0,lengthmax) + "...";
		}	
	}
	return texttolimit;
};

/**
 * 
 */
$(document).ready(
	function(){
		loadDataWelcomeTab();
		quickListSearch();
		mainTabNavigation();
		loadFoldersList();
		manageAjaxLoading();
		topMenuNavigation();
	}
);

/**
 * Fonction de gestion de l'affichage du GIF de chargement
 */
function manageAjaxLoading(){
	$(document).ajaxStart(				
		function(){
			$("body header nav div.loader-box").show();
		}
	);
	$(document).ajaxStop(
		function(){
			$("body header nav div.loader-box").hide();
		}
	);
	$(document).ajaxError(
		function(){
			$("body header nav div.loader-box").hide();
		}
	);	
}

/**
 * Fonction de recherche pour les composants box
 */
function quickListSearch(){
	$('input.easy-search-box').keyup(
		function(){
			var textSearch = $(this).val();
			$(this).parent().parent().find('article li a').each(
				function(){
					var indexTextSearch = $(this).html().toLowerCase().indexOf(textSearch.toLowerCase());
					if(indexTextSearch >= 0){
						$(this).parent().show();
					}else{
						$(this).parent().hide();
					}
				}
			);
		}
	);
}

/**
 * Fonction de création des onglets
 */
function createTab(tabid, tabname, maincontent, activityContent, propertyContent){
	
	if($("#"+tabid).exists()){
		$("#"+tabid +  " button.close-btn-box-tab").click();
	}
	
	$("section.content section.top-content div.main-content div header").append(
			"<div class='box-tab' id='"+ tabid + "' title='" + tabname +"'>" +
				"<button class='close-btn-box-tab'></button>" + 
				limitLength(tabname,25) +
			"</div>"
	);
	
	$("section.content section.top-content div.main-content article").append(
			maincontent
	);
	
	$("section.content section.top-content aside div article").append(
			activityContent
	);
	
	$("section.content section.bottom-content aside div article").append(
			propertyContent
	);
	
	mainTabNavigation();
	$("#" + tabid).click();
	
	tabMoveClickRight();
}

/**
 * Fonction de déplacement des onglets sur click gauche
 */
function tabMoveClickLeft(){
	
	searchAndShowLeftTab();
	
	var headerMainContentSize = $("div.main-content div.box header").width();
	var tabsSize = computeTabsSize();
	
	if(tabsSize > headerMainContentSize){
		searchAndHideRightTab();
		tabsSize = computeTabsSize();
	}
	
	while(tabsSize > headerMainContentSize){
		searchAndHideRightTab();
		searchAndShowLeftTab();
		tabsSize = computeTabsSize();
	}
	activeLeftArrowTab();
	activeRightArrowTab();
}

/**
 * Fonction de déplacement des onglets sur click droit
 */
function tabMoveClickRight(){
	
	searchAndShowRightTab();
	
	var headerMainContentSize = $("div.main-content div.box header").width();
	var tabsSize = computeTabsSize();
	
	if(tabsSize > headerMainContentSize){
		searchAndHideLeftTab();
		tabsSize = computeTabsSize();
	}
	
	while(tabsSize > headerMainContentSize){
		searchAndHideLeftTab();
		searchAndShowRightTab();
		tabsSize = computeTabsSize();
	}
	activeLeftArrowTab();
	activeRightArrowTab();
}

/**
 * Fonction de calcul de la taille des onglets
 */
function computeTabsSize(){
	var tabsSize = 0;
	var tabList = $("div.main-content div.box header div.box-tab");
	//+57 correspond au Marign gauche l'épassaire des bordures et padding gauche et droite
	$.each(tabList, function(i, tab){
		if($(tab).css("display") === "block"){
			tabsSize += ($(tab).width() + 57);
		}
	});
	//+48 correspond au fleche gauche et droite
	tabsSize += 48;
	return tabsSize;
} 

/**
 * Fonction de recherche et d'affichage de l'onglet de droite
 */
function searchAndShowRightTab(){
	var tabList = $("div.main-content div.box header div.box-tab");
	var startSearchTabDisplayNone = false;	
	$.each(tabList, function(i, tab){
		if($(tab).css("display") === "block"){
			startSearchTabDisplayNone =  true;
		}
		if(startSearchTabDisplayNone){
			if($(tab).css("display") === "none"){
				$(tab).show();
				startSearchTabDisplayNone=false;
			}
		}
	});
}

/**
 * Fonction de recherche et masquage de l'onglet de droite
 */
function searchAndHideRightTab(){
	var tabList = $("div.main-content div.box header div.box-tab");
	var startSearchLastDisplayBlock = false;
	var tabFound = false;
	var tabPrev=null;
	$.each(tabList, function(i, tab){
		if(startSearchLastDisplayBlock){
			if($(tab).css("display") === "none"){
				$(tabPrev).hide();
				startSearchLastDisplayBlock=false;
				tabFound=true;
			}
			tabPrev=tab;
		}
		if($(tab).css("display") === "block"){
			startSearchLastDisplayBlock =  true;
			tabPrev=tab;
		}
	});
	if(!tabFound){
		$(tabPrev).hide();
	}
}

/**
 * Fonction de recherche et d'afficahge de l'onglet de gauche
 */
function searchAndShowLeftTab(){
	var tabList = $("div.main-content div.box header div.box-tab");
	var stopSearchLastDisplayBlock = false;
	var tabPrev=null;
	$.each(tabList, function(i, tab){
		if(!stopSearchLastDisplayBlock){			
			if($(tab).css("display") === "block"){
				stopSearchLastDisplayBlock =  true;
				$(tabPrev).show();
			}
		}
		if($(tab).css("display") === "none"){
			tabPrev=tab;
		}
	});
}

/**
 * Fonction de recherche et masquage de l'onglet de gauche
 */
function searchAndHideLeftTab(){
	var tabList = $("div.main-content div.box header div.box-tab");
	var stopSearchLastDisplayBlock = false;
	$.each(tabList, function(i, tab){
		if(!stopSearchLastDisplayBlock){
			if($(tab).css("display") === "block"){
				stopSearchLastDisplayBlock =  true;
				$(tab).hide();
			}
		}
	});
}

/**
 * Gestion du curseur gauche onglet  
 */
function activeLeftArrowTab(){
	var tabList = $("div.main-content div.box header div.box-tab");
	var diplayNoneTabExist = false;	
	var startSearchFirstDisplayNoneTab = true;
	$.each(tabList, function(i, tab){
		if(startSearchFirstDisplayNoneTab){
			if($(tab).css("display") === "none"){
				diplayNoneTabExist=true;
				startSearchFirstDisplayNoneTab = false;
			}
			if($(tab).css("display") === "block"){
				startSearchFirstDisplayNoneTab = false;
			}
		}
	});
	if(diplayNoneTabExist){
		$("div.box header button.nav-left-box-tab").css("cursor","pointer");
		$("div.box header button.nav-left-box-tab").unbind().click(
			function(){
				tabMoveClickLeft();
			}
		);
	}else{
		$("div.box header button.nav-left-box-tab").css("cursor","default");
		$("div.box header button.nav-left-box-tab").unbind();
	}
}

function activeRightArrowTab(){
	var tabList = $("div.main-content div.box header div.box-tab");
	var diplayNoneTabExist = false;	
	var startSearchLastDisplayBlockTab = false;
	$.each(tabList, function(i, tab){
		if(startSearchLastDisplayBlockTab){
			if($(tab).css("display") === "none"){
				diplayNoneTabExist=true;
				startSearchLastDisplayBlockTab = false;
			}
		}
		if($(tab).css("display") === "block"){
			startSearchLastDisplayBlockTab = true;
		}
	});
	if(diplayNoneTabExist){
		$("div.box header button.nav-right-box-tab").css("cursor","pointer");
		$("div.box header button.nav-right-box-tab").unbind().click(
			function(){
				tabMoveClickRight();
			}
		);
	}else{
		$("div.box header button.nav-right-box-tab").css("cursor","default");
		$("div.box header button.nav-right-box-tab").unbind();
	}
}

/**
 * Fonction de navigation entre onglets
 */
function mainTabNavigation(){
	/** Navigation entre onglet */
	$("div.box-tab").unbind().click(
		function(){
			var tabId = $(this).attr("id");
			
			/** activation/desactivation onglet */
			$("section.content").find("div.box-tab").removeClass("active");
			$("section.content").find("#"+tabId).addClass("active");
			
			/** Navigation article */
			$("section.content").find("div.article-content").removeClass("active");
			$("section.content").find("#content-" + tabId).addClass("active");
			
			/**Navigation activit� */
			$("section.content").find("div.activity-content").removeClass("active");
			$("section.content").find("#activity-" + tabId).addClass("active");
						
			/**Navigation propriete */
			$("section.content").find("div.property-content").removeClass("active");
			$("section.content").find("#property-" + tabId).addClass("active");
		}
	);
	
	/** Fermeture onglet */
	$("button.close-btn-box-tab").unbind().click(
		function(){
			var tabId = $(this).parent().attr("id");
			$("section.content").find("#"+tabId).remove();
			$("section.content").find("#content-" + tabId).remove();
			$("section.content").find("#activity-" + tabId).remove();
			$("section.content").find("#property-" + tabId).remove();
			
			tabMoveClickRight();
			
			$("section.top-content div.main-content div.box header").find("div.box-tab").filter(":last").click();
		}
	);
}

/**
 * Fonction de gestion du tableau liste des documents
 */
function manageFilesListArticleContent(target){
	$(target).DataTable({
		"language": {
            "url": "js/plugin/datatables/French.json"
        },
        "scrollY":        "auto",
        "scrollCollapse": true,
        "paging":         false,
        "autoWidth": false,
        "columnDefs": [
                       { "orderable": false, "width": "0px", "targets": 0 }
                     ],
        "order": [[ 1, "asc" ]]
    });
}

/**
 * Fonction de navigation du menu haut droit
 */
function topMenuNavigation(){
	
	$("div.global-settings").click(
		function(){
			loadGlobalSettingsTab();
		}
	);
	$("div.account").click(
		function(){
			loadAccountTab();
		}
	);
	$("div.event-list").click(
		function(){
			loadEventListTab();
		}
	);
	$("div.favorites").click(
		function(){
			loadFavoritesTab();
		}
	);
}

/**
 * Fonction de chargement des propriétés applicatives personnalisées
 */
function loadGlobalSettingsTab(){
	var htmlGlobalSettingsMainContent = buildGlobalSettingsMainContent();
	var htmlGlobalSettingsActivityContent = buildGlobalSettingsActivityContent();
	var htmlGlobalSettingsPropertyContent = buildGlobalSettingsPropertyContent();
	var tabid = "global-settings-tab";
	var tabname = "Préférences";
	createTab(tabid, tabname, htmlGlobalSettingsMainContent, htmlGlobalSettingsActivityContent, htmlGlobalSettingsPropertyContent);
}

function buildGlobalSettingsMainContent(){
	
	var htmlGlobalSettingsMainContent = "<div id='content-global-settings-tab' class='article-content'>" +
											"<div class='title-content-home'>Préférences</div> " +
										"</div>";
	
	return htmlGlobalSettingsMainContent;
}


function buildGlobalSettingsActivityContent(){
	var htmlGlobalSettingsActivityContent = "<div id='activity-global-settings-tab' class='activity-content'>" +
											"</div>";
	return htmlGlobalSettingsActivityContent;
}

function buildGlobalSettingsPropertyContent(){
	var htmlGlobalSettingsPropertyContent = "<div id='property-global-settings-tab' class='property-content'>" +
											"</div>";
	return htmlGlobalSettingsPropertyContent;
}

/**
 * Fonction de chargement des informations du profil personnel
 */
function loadAccountTab(){
	var htmlAccountMainContent = buildAccountMainContent();
	var htmlAccountActivityContent = buildAccountActivityContent();
	var htmlAccountPropertyContent = buildAccountPropertyContent();
	var tabid = "account-tab";
	var tabname = "Profil";
	createTab(tabid, tabname, htmlAccountMainContent, htmlAccountActivityContent, htmlAccountPropertyContent);
}

function buildAccountMainContent(){
	
	var htmlAccountMainContent = "<div id='content-account-tab' class='article-content'>" +
									"<div class='title-content-home'>Profil</div> " +
								"</div>";
	
	return htmlAccountMainContent;
}

function buildAccountActivityContent(){
	var htmlAccountActivityContent = "<div id='activity-account-tab' class='activity-content'>" +
									"</div>";
	return htmlAccountActivityContent;
}

function buildAccountPropertyContent(){
	var htmlAccountPropertyContent = "<div id='property-account-tab' class='property-content'>" +
									"</div>";
	return htmlAccountPropertyContent;
}

/**
 * Fonction de chargement de la liste des évenements
 */
function loadEventListTab(){
	var htmlEventListMainContent = buildEventListMainContent();
	var htmlEventListActivityContent = buildEventListActivityContent();
	var htmlEventListPropertyContent = buildEventListPropertyContent();
	var tabid = "event-list-tab";
	var tabname = "Evénements";
	createTab(tabid, tabname, htmlEventListMainContent, htmlEventListActivityContent, htmlEventListPropertyContent);
}

function buildEventListMainContent(){
	
	var htmlEventListMainContent = "<div id='content-event-list-tab' class='article-content'>" +
										"<div class='title-content-home'>Evénements</div> " +
									"</div>";
	
	return htmlEventListMainContent;
}

function buildEventListActivityContent(){
	var htmlEventListActivityContent = "<div id='activity-event-list-tab' class='activity-content'>" +
									"</div>";
	return htmlEventListActivityContent;
}

function buildEventListPropertyContent(){
	var htmlEventListPropertyContent = "<div id='property-event-list-tab' class='property-content'>" +
									"</div>";
	return htmlEventListPropertyContent;
}

/**
 * Fonction de chargement de la liste des favoris
 */
function loadFavoritesTab(){
	var htmlFavoritesMainContent = buildFavoritesMainContent();
	var htmlFavoritesActivityContent = buildFavoritesActivityContent();
	var htmlFavoritesPropertyContent = buildFavoritesPropertyContent();
	var tabid = "favorites-tab";
	var tabname = "Favoris";
	createTab(tabid, tabname, htmlFavoritesMainContent, htmlFavoritesActivityContent, htmlFavoritesPropertyContent);
}

function buildFavoritesMainContent(){
	
	var htmlFavoritesMainContent = "<div id='content-favorites-tab' class='article-content'>" +
										"<div class='title-content-home'>Favoris</div> " +
									"</div>";
	
	return htmlFavoritesMainContent;
}

function buildFavoritesActivityContent(){
	var htmlFavoritesActivityContent = "<div id='activity-favorites-tab' class='activity-content'>" +
									"</div>";
	return htmlFavoritesActivityContent;
}

function buildFavoritesPropertyContent(){
	var htmlFavoritesPropertyContent = "<div id='property-favorites-tab' class='property-content'>" +
									"</div>";
	return htmlFavoritesPropertyContent;
}

/**
 * Fonction de remplissage de la box dossier
 */
function loadFoldersList(){
	$.ajax({
		url : "/gednet-business/resources/folder-resource/folders",
		type : "GET",
		dataType: "json",
		contentType: 'application/json',
        success: function(lFolderResources){
        	var htmlFoldersList = "<ul class='folder-nav'>";
        	var curLevel = 1;
        	var first = true;
        	$.each(lFolderResources, function(i,folderResource){
        		var level = parseInt(folderResource.level);
        		if(level === curLevel){
        			if(!first){
            			htmlFoldersList += "</li>";
            		}else{
            			first = false;
            		}
        			htmlFoldersList += "<li>" + 
        									"<div>" + 
        										"<a href='javascript:void(0);' folder-id='" + folderResource.folderid + "' created='" + folderResource.created + "'>" + folderResource.name + "</a>" +
        										"<button class='folder-view' folder-id='" + folderResource.folderid + "'></button>" +
        										"<button class='folder-add' folder-id='" + folderResource.folderid + "'></button>" +
        									"</div>";
        			curLevel = level;
        		}else if(level > curLevel){
        			htmlFoldersList += "<ul>" + 
						        			"<li>" + 
											"<div>" + 
												"<a href='javascript:void(0);' folder-id='" + folderResource.folderid + "' created='" + folderResource.created + "'>" + folderResource.name + "</a>" +
												"<button class='folder-view' folder-id='" + folderResource.folderid + "'></button>" +
												"<button class='folder-add' folder-id='" + folderResource.folderid + "'></button>" +
											"</div>";
        			curLevel = level;
        		}else if(level < curLevel){
        			var levelDiff = curLevel - level;
        			for(var i = 0; i < levelDiff; i++){
        				htmlFoldersList += "</li>" +
        								"</ul>";
        			}
        			htmlFoldersList += "<li>" + 
											"<div>" + 
												"<a href='javascript:void(0);' folder-id='" + folderResource.folderid + "' created='" + folderResource.created + "'>" + folderResource.name + "</a>" +
												"<button class='folder-view' folder-id='" + folderResource.folderid + "'></button>" +
												"<button class='folder-add' folder-id='" + folderResource.folderid + "'></button>" +
											"</div>";
        			curLevel = level;
        		}
        	});        		
        	
        	$("div.folder-box article").html(htmlFoldersList);
        	
        	addFolder();
        	
        	loadFileListByFolder();
        	loadDetailFileListByFolder();
        }
	});
}

/**
 * Fonction d'ajout d'un dossier
 */
function addFolder(){
	$("div.folder-box article ul.folder-nav").append(
			"<li>" + 
				"<div>" + 
					"<input type='text' class='folder-add-input' placeholder='ajouter un dossier ...' />" +
				"</div>" +
			"</li>");
	
	$("div.folder-box article ul.folder-nav button.folder-add").click(
		function(){
			var ulFirst = $(this).parent().parent().find("ul").filter(":first");
			if(ulFirst.exists()){
				ulFirst.prepend(
						"<li>" + 
							"<div>" + 
								"<input type='text' class='folder-add-input' placeholder='ajouter un dossier ...' />" +
							"</div>" +
						"</li>"
				);
			}else{
				$(this).parent().parent().append(
						"<ul>" + 
							"<li>" + 
								"<div>" + 
									"<input type='text' class='folder-add-input' placeholder='ajouter un dossier ...' />" +
								"</div>" +
							"</li>" +
						"</ul>"
				);
			}
			
			addFolderSend();
			
			$(this).parent().parent().find("input.folder-add-input")
			.focus()
			.blur(
				function(){
					$(this).parent().parent().remove();
				}
			);
		}
	);
	
	addFolderSend();
}

/**
 * Fonction d'ajout d'un dossier - partie émission
 */
function addFolderSend(){
	$('div.folder-box article ul.folder-nav input.folder-add-input').unbind().blur(
			function(){
				$(this).val("");
			}
		).keyup(
			function(e){
				var code = e.keyCode || e.which;				
				 if(code == 13) {
				 	var folderResource = new Object();
					folderResource.name = $(this).val();
					folderResource.parentFolderid = 1;
					if($(this).parent().parent().parent().parent().tagName() === "LI"){
						var parentfolder = $(this).parent().parent().parent().parent().find("a").filter(":first");
						if(parentfolder.exists()){
							folderResource.parentFolderid = parentfolder.attr("folder-id");
						}
					}
					$.ajax({
							url : "/gednet-business/resources/folder-resource/folders",
							type : "POST",
							dataType: "json",
							data : JSON.stringify(folderResource),
							contentType: 'application/json',
					        success: function(){
					        	loadFoldersList();
					        }
					 });
				 }
			}
		);
}

/**
 * Fonction permettant de récupérer les dossiers parents d'un dossier
 */
function displayParentsFolder(folderid, target){
	$.ajax({
		url : "/gednet-business/resources/folder-resource/folders/"+folderid+"/parents",
		type : "GET",
		dataType: "json",
		contentType: 'application/json',
        success: function(lFoldersResource){
        	var htmlFoldersResource = "";
        	$.each(lFoldersResource, function(i,FolderResource){
        		htmlFoldersResource += " > " + FolderResource.name;
        	});
        	$(target).html(htmlFoldersResource);
        }
	});
}

/**
 * Fonctions de chargement des données de l'ongle d'accueil
 */
function loadDataWelcomeTab(){
	var htmlDataWelcomeTabMainContent = buildDataWelcomeTabMainContent();
	$("#content-home-tab").html(htmlDataWelcomeTabMainContent);
	buildDocumentGraphWelcomeTab();
}

function buildDataWelcomeTabMainContent(){
	var htmlDataWelcomeTabMainContent = 	"<div class='title-content-home'>Tableau de bord</div> " +
											"<div class='subtitle-content-home'>Bienvenue dans l'application GedNet</div> " +
											"<div class='statistics-meter-content-home'> " +
												"<div class='box-statistics-meter-content-home'> " +
													"<div class='subbox-statistics-meter-content-home'> " +
														"<div class='num-box-statistics-meter-content-home'>100</div> " +
														"<div class='title-box-statistics-meter-content-home'>Documents</div>  " +
													"</div> " +
												"</div> " +
												"<div class='box-statistics-meter-content-home'> " +
													"<div class='subbox-statistics-meter-content-home color2'> " +
														"<div class='num-box-statistics-meter-content-home'>23</div> " +
														"<div class='title-box-statistics-meter-content-home'>Dossiers</div>  " +
													"</div> " +
												"</div> " +
												"<div class='box-statistics-meter-content-home'> " +
													"<div class='subbox-statistics-meter-content-home color3'> " +
														"<div class='num-box-statistics-meter-content-home'>84</div> " +
														"<div class='title-box-statistics-meter-content-home'>MO</div> " +
													"</div>  " +
												"</div> " +
												"<div class='box-statistics-meter-content-home'> " +
													"<div class='subbox-statistics-meter-content-home color4'> " +
														"<div class='num-box-statistics-meter-content-home'>5</div> " +
														"<div class='title-box-statistics-meter-content-home'>Utilisateurs</div> " +
													"</div> " +
												"</div> " +
											"</div>"+
											"<div class='statistics-box-content-home half' id='document-graph-home'>"+
												"<div class='title-statistics-box-content-home'>"+
													"Evolution du nombre de documents"+
												"</div>"+
												"<div class='graph-statistics-box-content-home'>"+
												"</div>"+
											"</div>";
	return htmlDataWelcomeTabMainContent;
}

function buildDocumentGraphWelcomeTab(){
	var d5 = [];
	for (var i = 0; i < 14; i += 0.5) {
		d5.push([i, Math.sqrt(i)]);
	} 
	
	$.plot("#document-graph-home div.graph-statistics-box-content-home", [{
		data: d5,
		lines: { show: true },
		points: { show: false }
		}]); 
}

/**
 * Fonction de chargement du tableau détail des dossiers
 */
function loadDetailFileListByFolder(){
	$("ul.folder-nav button.folder-view").click(
		function(){
			var folderid = $(this).attr("folder-id");
			var folderResource=new Object();
			folderResource.name = $(this).parent().find("a").filter(":first").html();
			folderResource.created = $(this).parent().find("a").filter(":first").attr("created");
			var obja = $(this).parent().find("a").filter(":first");
			$.ajax({
				url : "/gednet-business/resources/folder-resource/folders/"+folderid+"/files",
				type : "GET",
				dataType: "json",
				contentType: 'application/json',
		        success: function(lFilesResource){
		        	var htmlListDetailFileMainContent = buildDetailFileMainContent(folderid, lFilesResource);
		        	var htmlDetailFileActivityContent = buildDetailFileActivityContent(folderid);
		        	var htmlDetailfilePropertyContent = buildDetailFilePropertyContent(folderid, folderResource);
		        	var tabid = "folders-" + folderid + "-tab";
		        	var tabname = folderResource.name;
		        	createTab(tabid, tabname, htmlListDetailFileMainContent, htmlDetailFileActivityContent, htmlDetailfilePropertyContent);
		        	displayParentsFolder(folderid, "#property-folders-" + folderid + "-tab div.folder-name-item-property-content");
		        	displayParentsFolder(folderid, "#content-folders-" + folderid + "-tab div.folder-name-item-article-content");
		        	manageFilesListArticleContent("#content-folders-" + folderid + "-tab div table.table-files-list-item-article-content");
		        	loadFileContent("#content-folders-" + folderid + "-tab tbody tr");
		        	$(obja).click();
		        }
			});
		}
	);
}

function buildDetailFileMainContent(folderid, lFilesResource){
	var htmlListDetailFileMainContent = "";
	$.each(lFilesResource, function(i,fileResource){
		var fileclassnav = getMimeTypeClass(fileResource.mimeTypeFileResource.mimetypefileid) + "-nav";
		var sizeStr="";
        var sizeKB = fileResource.size/1024;
        if(parseInt(sizeKB) > 1024)
        {
            var sizeMB = sizeKB/1024;
            sizeStr = sizeMB.toFixed(2)+" MB";
        }
        else
        {
            sizeStr = sizeKB.toFixed(2)+" KB";
        }        
        
        htmlListDetailFileMainContent +=  "<tr file-id='" +  fileResource.fileid + "'>" + 
											"<td class='" + fileclassnav + "'></td>" + 
											"<td>" + fileResource.name + "</td>" + 
											"<td>" + fileResource.mimeTypeFileResource.mimetypefilename + "</td>" + 
											"<td>" + fileResource.lastmodified + "</td>" + 
											"<td>" + sizeStr + "</td>" +
											"<td>" + fileResource.versionfile + "</td>" + 
										  "</tr>";
			
	});
	var htmlDetailFileMainContent = "<div class='article-content file-viewer' id='content-folders-" + folderid + "-tab'>" +
										"<div class='folder-name-item-article-content'></div>" +
										"<div class='files-list-item-article-content'>" +
											"<table class='table-files-list-item-article-content'>" +
												"<thead>" +
													"<tr>" +
														"<th></th>" +
														"<th>Nom</th>" +
														"<th>Type</th>" +
														"<th>Dernière modification</th>" +
														"<th>Poids</th>" +
														"<th>Version</th>" +
													"</tr>" +
												"</thead>" +
												"<tbody>" +
													htmlListDetailFileMainContent +
												"</tbody>" +
											 "</table>" +
										 "</div>" +
									"</div>";
	return htmlDetailFileMainContent;
}

function buildDetailFileActivityContent(folderid){
	var htmlDetailFileActivityContent = "<div class='activity-content' id='activity-folders-" + folderid + "-tab'> " +
										"<ul class='news-item-activity'> " +
											"<li>Création du dossier : 5 novembre 2015</li> " +
											"<li> " +
												"Mise à jour dossier : 10 janvier 2016<br /> " +
												"Modification du nom 'Dossier 4.2.2' en 'Dossier 4.2.1' " +
											"</li> " +
										"</ul> " +
									"</div>";
	return htmlDetailFileActivityContent;
}

function buildDetailFilePropertyContent(folderid, folderResource){
	var htmlDetailfilePropertyContent = "<div class='property-content' id='property-folders-" + folderid + "-tab'> " +
										"<div class='icon-file-property-content folder'></div> " +
										"<div class='item-property-content'> " +
											"<div class='folder-name-item-property-content'></div> " +
											"<div class='file-name-item-property-content'>" + folderResource.name + "</div> " +
											"<div class='file-version-item-property-content'>Création : " + folderResource.created + "</div> " +
											"<div class='file-action-item-property-content'> " +
												"<div class='remove-file-action-item-property-content'></div> " +
												"<div class='add-file-action-item-property-content'></div> " +
											"</div>" +
										"</div> " +
									"</div>";
	return htmlDetailfilePropertyContent;
}



/**
 * Fonction sur le chargement des fichiers par dossier
 */
function loadFileListByFolder(){
	$("ul.folder-nav a").click(
		function(){
			var folderid = $(this).attr("folder-id");
			$.ajax({
				url : "/gednet-business/resources/folder-resource/folders/"+folderid+"/files",
				type : "GET",
				dataType: "json",
				contentType: 'application/json',
		        success: function(lFilesResource){
		        	var htmlFilessList = "<ul class='file-nav'>";
		        	$.each(lFilesResource, function(i,fileResource){
		        		var fileclassnav = getMimeTypeClass(fileResource.mimeTypeFileResource.mimetypefileid) + "-nav";
		        		htmlFilessList += "<li class='" + fileclassnav + "'>" +
		        							"<a href='javascript:void(0);' file-id='" + fileResource.fileid + "' title='" + fileResource.name + "'>" + fileResource.name + "</a>" +
		        						  "</li>";
		        			
		        	});
		        	
		        	if(lFilesResource.length == 0){
		        		htmlFilessList += "<li>" +
												"<span class='no-file-folder'>Aucun fichier</span>" +
										  "</li>";
		        	}
		        	
		        	htmlFilessList += "</ul>";
		        	
		        	$("div.file-box article").html(htmlFilessList); 
		        	
		        	$("div.file-box article").attr("folder-id", folderid);
		        			        					    
		        	buildUploadzone();
		        	
		        	loadFileContent("ul.file-nav li a");
		        	
		        }
			});
		}
	);
}

/**
 * Fonction de management des afficahges de fichier. 
 */
function loadFileContent(target){
	$(target).click(
		function(){
			var fileid = $(this).attr("file-id");
			var folderid=$("div.file-box article").attr("folder-id");
			
			$.ajax({
				url : "/gednet-business/resources/folder-resource/folders/"+folderid+"/files/"+fileid,
				type : "GET",
				dataType: "json",
				contentType: 'application/json',
		        success: function(fileResource){
		        	var htmlFileMainContent = buildFileMainContent(folderid, fileid, fileResource);
		        	var htmlFileActivityContent = buildFileActivityContent(folderid, fileid);
		        	var htmlfilePropertyContent = buildFilePropertyContent(folderid, fileid, fileResource);
		        	var tabid = "file-" + fileid + "-tab";
		        	var tabname = fileResource.name;
		        	createTab(tabid, tabname, htmlFileMainContent, htmlFileActivityContent, htmlfilePropertyContent);
		        	displayParentsFolder(folderid, "#property-file-" + fileid + "-tab div div.folder-name-item-property-content");
		        	displayFileTags(folderid, fileid, "#property-file-" + fileid + "-tab div div.file-tags-item-property-content");
		        	displayFileActivity(folderid, fileid, "#activity-file-" + fileid + "-tab");
		        	manageActionsFile("#property-file-" + fileid + "-tab");
		        }
			});
		}	
	);
}

/**
 * 
 */
function buildFileMainContent(folderid, fileid, fileResource){
	var htmlFileMainContent = "<div class='article-content file-viewer' id='content-file-" + fileid + "-tab'>" +
										"<iframe class='viewer-article-content' " +
											"src='js/plugin/viewerjs-0.5.8/ViewerJS/index.html#/gednet-business/resources/folder-resource/folders/"+folderid+"/files/" + fileid +  "/content/" + fileResource.name + "' " +
											"allowfullscreen webkitallowfullscreen> " +
										"</iframe> " +
									"</div>";
	return htmlFileMainContent;
}

function buildFileActivityContent(folderid, fileid){
	var htmlFileActivityContent = "<div class='activity-content' id='activity-file-" + fileid + "-tab'> " +
									"</div>";
	return htmlFileActivityContent;
}

function buildFilePropertyContent(folderid, fileid, fileResource){
	var fileclassmimetype = getMimeTypeClass(fileResource.mimeTypeFileResource.mimetypefileid);
	var htmlfilePropertyContent = "<div class='property-content' id='property-file-" + fileid + "-tab'> " +
										"<div class='icon-file-property-content " + fileclassmimetype + "'></div> " +
										"<div class='item-property-content'> " +
											"<div class='folder-name-item-property-content'></div> " +
											"<div class='file-name-item-property-content'>" + fileResource.name + "</div> " +
											"<div class='file-version-item-property-content'>Version: " + fileResource.versionfile + " | Création : " + fileResource.created + " | Dernière modification : " + fileResource.lastmodified + "</div> " +
											"<div class='file-tags-item-property-content'></div> " +
											"<div class='file-action-item-property-content'> " +
												"<button class='remove-file-action-item-property-content' file-id='" + fileid + "' folder-id='" + folderid + "' title='Supprimer le document'></button> " +
												"<button class='update-file-action-item-property-content' file-id='" + fileid + "' folder-id='" + folderid + "' title='Mettre à jour le document'></button> " +
												"<button class='download-file-action-item-property-content' file-id='" + fileid + "' folder-id='" + folderid + "' title='Télécharger le document'></button> " +
												"<button class='add-favorite-file-action-item-property-content' file-id='" + fileid + "' folder-id='" + folderid + "' title='Ajouter le document au favoris'></button> " +
											"</div>" +
										"</div> " +
										"<div class='rules-item-property-content'> " +
											"<div class='title-rules-item-property-content'>Droits sur le fichier</div> " +
											"<div class='load-rules-item-property-content'> " +
												"Document : <input type='radio' name='rules-file' id='public-rules-file' value='public' checked='checked'><label for='public-rules-file'>Public</label> " +
												"<input type='radio' name='rules-file' id='private-rules-file' value='private'><label for='private-rules-file'>Privé</label> " +
											"</div> " +
										"</div>" +
									"</div>";
	return htmlfilePropertyContent;
}


/**
 * Fonction pour le management des actions proprosés pour un fichier
 * @param target
 */
function manageActionsFile(target){
	$(target + " button.remove-file-action-item-property-content").click(
		function(){
			var fileid = $(this).attr("file-id");
			var folderid = $(this).attr("folder-id");
			$.confirm({
			    title: 'Suppression d\'un document',
			    content: 'Cliquer sur Valider pour supprimer définitivement le document',
			    confirmButton: 'Valider',
			    cancelButton: 'Annuler',
			    confirm: function(){
			    	$.ajax({
						url : "/gednet-business/resources/folder-resource/folders/"+folderid+"/files/"+fileid,
						type : "DELETE",
						dataType: "json",
						contentType: 'application/json',
				        success: function(response){
				        	$("#file-" + fileid + "-tab").find("button.close-btn-box-tab").click();
				        	$("ul.folder-nav a[folder-id=" + folderid + "]").click();
				        },
				        error: function(xhr, status, error) {
				        	$("#file-" + fileid + "-tab").find("button.close-btn-box-tab").click();
				        	$("ul.folder-nav a[folder-id=" + folderid + "]").click();
				        }
			    	});
			    },
			    cancel: function(){
			        
			    }
			});
		}
	);
	
	$(target + " button.download-file-action-item-property-content").click(
		function(){
			var fileid = $(this).attr("file-id");
			var folderid = $(this).attr("folder-id");
			var filename = $(this).parent().parent().find("div.file-name-item-property-content").html();
			
			window.location="/gednet-business/resources/folder-resource/folders/"+folderid+"/files/" + fileid +  "/content/" + filename;
		}
	);
}

/**
 * Affichage et gestion des tags d'un document
 * @param folderid
 * @param fileid
 * @param target
 */
function displayFileTags(folderid, fileid, target){
	$.ajax({
		url : "/gednet-business/resources/folder-resource/folders/"+folderid+"/files/"+fileid+"/tags",
		type : "GET",
		dataType: "json",
		contentType: 'application/json',
        success: function(lFileTagsResource){
        	var htmlFileTagsResource = "Tags: ";
        	$.each(lFileTagsResource, function(i,FileTagsResource){
        		htmlFileTagsResource += " <span filetags-id='" + FileTagsResource.filetagsid + "'>#" + FileTagsResource.tag + "</span><button class='delete-tags-item-property-content' filetags-id='" + FileTagsResource.filetagsid + "'></button>";
        	});
        	htmlFileTagsResource += "<input type='text' class='input-add-tags-item-property-content' placeholder='ajouter tag ...'/><!--<button class='add-tags-item-property-content'></button>-->";
        	
        	$(target).html(htmlFileTagsResource);
        	
        	$(target + " button.delete-tags-item-property-content").click(
        		function(){
        			var filetagsid = $(this).attr("filetags-id");
        			$.ajax({
        				url : "/gednet-business/resources/folder-resource/folders/"+folderid+"/files/"+fileid+"/tags/"+filetagsid,
        				type : "DELETE",
        				dataType: "json",
        				contentType: 'application/json',
        		        success: function(){
        		        	displayFileTags(folderid, fileid, target);
        		        }
        			});
        		}
        	);
        	
        	$(target + " input.input-add-tags-item-property-content").unbind().blur(
        			function(){
        				$(this).val("");
        			}
        		).keyup(
        			function(e){
        				var code = e.keyCode || e.which;				
        				 if(code == 13) {
        					var fileTagsResource = new Object();
        					fileTagsResource.tag = $(this).val();
        					$.ajax({
        							url : "/gednet-business/resources/folder-resource/folders/"+folderid+"/files/"+fileid+"/tags",
        							type : "POST",
        							dataType: "json",
        							data : JSON.stringify(fileTagsResource),
        							contentType: 'application/json',
        					        success: function(){
        					        	displayFileTags(folderid, fileid, target);
        					        }
        					 });
        				 }
        			}
        		);
        }
	});
}

function displayFileActivity(folderid, fileid, target){
	$.ajax({
		url : "/gednet-business/resources/folder-resource/folders/"+folderid+"/files/"+fileid+"/activity",
		type : "GET",
		dataType: "json",
		contentType: 'application/json',
        success: function(lFileActivityResource){
        	var htmlFileActivityResource = "<ul class='news-item-activity'> ";
        	
        	$.each(lFileActivityResource, function(i,FileActivityResource){
        		htmlFileActivityResource += "<li>"+FileActivityResource.created + " : " + FileActivityResource.event+"</li> ";
        	});
        	
        	htmlFileActivityResource += "</ul> ";
        	
        	$(target).html(htmlFileActivityResource);
        }	
	});
}

/**
 * Fonction utilisée pour positionner la bonne class css en fonction du mime type
 * @param mimetype
 * @returns {String}
 */
function getMimeTypeClass(mimetype){
	var fileclassnav = "default-file";
	switch($.trim(mimetype)){
		case 'application/pdf' :
			fileclassnav = 'reader-file';
			break;
		case 'application/vnd.adobe.x-mars' :
			fileclassnav = 'reader-file';
			break;
		case 'application/msword' :
			fileclassnav = 'word-file';
			break;
		case 'application/vnd.ms-excel' :
			fileclassnav = 'excel-file';
			break;
		case 'application/vnd.ms-powerpoint' :
			fileclassnav = 'powerpoint-file';
			break;
		case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' :
			fileclassnav = 'word-file';
			break;
		case 'application/vnd.oasis.opendocument.text' :
			fileclassnav = 'openoffice-writer-file';
			break;
		case 'application/vnd.oasis.opendocument.presentation' :
			fileclassnav = 'openoffice-writer-file';
			break;
		default:
			fileclassnav = "default-file";
	}
	return fileclassnav;
}

/**
 * Fonction pour créer la zone draganddrop sur file-nav
 */
function buildUploadzone(){
	$("div.file-box article").unbind();
	$("div.file-box div.dragdrop").unbind();
	$("div.file-box div.dragdrop").on('dragenter', function (e) 
	{
		e.stopPropagation();
		e.preventDefault();
		$("div.file-box div.dragdrop").css('display', 'block');
		$("div.file-box div.dragdrop").css('background-color', '#88A8A2');
	});
	$("div.file-box div.dragdrop").on('dragover', function (e) 
	{
		 e.stopPropagation();
		 e.preventDefault();
	});
	$("div.file-box div.dragdrop").on('drop', function (e) 
	{
		 $("div.file-box div.dragdrop").css('display', 'none');
		 e.preventDefault();
		 var files = e.originalEvent.dataTransfer.files;
	 
		 //We need to send dropped files to Server
		 handleFileUpload(files,$("div.file-box article ul.file-nav"));
	});
	$(document).on('drag', function (e) 
	{
		$("div.file-box div.dragdrop").css('display', 'block');
		e.stopPropagation();
		e.preventDefault();
	});
	$(document).on('dragenter', function (e) 
	{
		e.stopPropagation();
		e.preventDefault();
	});
	$(document).on('dragend', function (e) 
	{
		$("div.file-box div.dragdrop").css('display', 'none');	
		e.stopPropagation();
		e.preventDefault();
	});
	$(document).on('dragexit', function (e) 
	{
		$("div.file-box div.dragdrop").css('display', 'none');	
		e.stopPropagation();
		e.preventDefault();
	});
	$(document).on('dragover', function (e) 
	{
		e.stopPropagation();
		e.preventDefault();
		$("div.file-box div.dragdrop").css('background-color', '#6B797D');
		$("div.file-box div.dragdrop").css('display', 'block');
	});
	$(document).on('drop', function (e) 
	{
		e.stopPropagation();
		e.preventDefault();
		$("div.file-box div.dragdrop").css('display', 'none');	
	});
}

/**
 * Fonction de traitement de l'upload
 * @param files
 * @param obj
 */
function handleFileUpload(files,obj)
{
   for (var i = 0; i < files.length; i++) 
   {
        var fd = new FormData();
        fd.append('file', files[i]);
 
        var status = new createStatusbar(obj); //Using this we can set progress.
        status.setFileNameSize(files[i].name,files[i].size);
        sendFileToServer(fd,status);
 
   }
}

/**
 * Fonction de traitement de la bar de progression lors du chargement
 */
var rowCount=0;
function createStatusbar(obj)
{
     rowCount++;
     var row="odd";
     if(rowCount %2 ==0) row ="even";
     this.statusbar = $("<li class='default-file-nav statusbar "+row+"'></li>");
     this.filename = $("<a class='filename'></a>").appendTo(this.statusbar);
     this.size = $("<div class='filesize'></div>").appendTo(this.statusbar);
     this.progressBar = $("<div class='progressBar'><div></div></div>").appendTo(this.statusbar);
     this.abort = $("<div class='abort'>Abort</div>").appendTo(this.statusbar);
     obj.append(this.statusbar);
 
    this.setFileNameSize = function(name,size)
    {
        var sizeStr="";
        var sizeKB = size/1024;
        if(parseInt(sizeKB) > 1024)
        {
            var sizeMB = sizeKB/1024;
            sizeStr = sizeMB.toFixed(2)+" MB";
        }
        else
        {
            sizeStr = sizeKB.toFixed(2)+" KB";
        }
 
        this.filename.html(name);
        this.size.html(sizeStr);
    };
    this.setProgress = function(progress)
    {       
        var progressBarWidth =progress*this.progressBar.width()/ 100;  
        this.progressBar.find('div').animate({ width: progressBarWidth }, 10).html(progress + "% ");
        if(parseInt(progress) >= 100)
        {
            this.abort.hide();
        }
    };
    this.setAbort = function(jqxhr)
    {
        var sb = this.statusbar;
        this.abort.click(function()
        {
            jqxhr.abort();
            sb.hide();
        });
    };
}

/**
 * Fonction d'envoi du fichier au serveur
 * @param formData
 * @param status
 */
function sendFileToServer(formData,status)
{
	var folderid=$("div.file-box article").attr("folder-id");
    var uploadURL ="/gednet-business/resources/folder-resource/folders/"+folderid+"/files"; //Upload URL
    //var extraData ={}; //Extra Data.
    var jqXHR=$.ajax({
            xhr: function() {
            var xhrobj = $.ajaxSettings.xhr();
            if (xhrobj.upload) {
                    xhrobj.upload.addEventListener('progress', function(event) {
                        var percent = 0;
                        var position = event.loaded || event.position;
                        var total = event.total;
                        if (event.lengthComputable) {
                            percent = Math.ceil(position / total * 100);
                        }
                        //Set progress
                        status.setProgress(percent);
                    }, false);
                }
            return xhrobj;
        },
        url: uploadURL,
        type: "POST",
        contentType:false,
        processData: false,
        cache: false,
        data: formData,
        success: function(data){
            status.setProgress(100);
            $("ul.folder-nav a[folder-id=" + folderid + "]").click();
        }
    }); 
 
    status.setAbort(jqXHR);
}