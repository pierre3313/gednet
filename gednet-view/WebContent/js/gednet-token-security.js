/**
 * 
 */
$(document).ready(
	function(){		
		setBearedToken();
	}
);

function setBearedToken(){
	var token = $.cookie("gednet_token");
	$.ajaxSetup({
		headers:{Authorization : token}
	});
}