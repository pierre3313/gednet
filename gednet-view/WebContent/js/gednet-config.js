/**
 * 
 */

var GEDNET_BUSINESS_HTTP_URL = 'http://localhost:7001';
var GEDNET_BUSINESS_HTTPS_URL = 'http://localhost:7001';
var GEDNET_SECURE_HTTP_URL =  'http://localhost:7001';
var GEDNET_SECURE_HTTPS_URL =  'http://localhost:7001';

$(document).ready(
	function(){
		$(document).ajaxSend(
			function(e, xhr, settings){
				if(settings.url.indexOf('/gednet-business', 0) !== -1)
						settings.url = GEDNET_BUSINESS_HTTP_URL + settings.url;
			}
		);
	}
);