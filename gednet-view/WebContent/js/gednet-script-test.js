jQuery.fn.exists = function(){return this.length>0;};

/**
 * 
 */
$(document).ready(
	function(){
		quickListSearch();
		mainTabNavigation();
		manageFilesListArticleContent();
		buildDocumentGraphWelcomeTab();
	}
);

function buildDocumentGraphWelcomeTab(){
	var d5 = [];
	for (var i = 0; i < 14; i += 0.5) {
		d5.push([i, Math.sqrt(i)]);
	} 
	
	$.plot("#document-graph-home div.graph-statistics-box-content-home", [{
		data: d5,
		lines: { show: true },
		points: { show: false }
		}]); 
}

/**
 * Fonction de remplissage de la box dossier
 */
function loadFoldersList(){
	$.ajax({
		url : "resources/folder-resource/folders",
		type : "GET",
		dataType: "json",
		contentType: 'application/json',
        success: function(lFolderResources){
        	var htmlFoldersList = "";
        	var curLevel = 0;
        	$.each(lFolderResources, function(i,folderResource){
        		
        	});
        }
	});
}

/**
 * Fonction de recherche pour les composants box
 */
function quickListSearch(){
	$('input.easy-search-box').keyup(
		function(){
			var textSearch = $(this).val();
			$(this).parent().parent().find('article li a').each(
				function(){
					var indexTextSearch = $(this).html().toLowerCase().indexOf(textSearch.toLowerCase());
					if(indexTextSearch >= 0){
						$(this).parent().show();
					}else{
						$(this).parent().hide();
					}
				}
			);
		}
	);
}

/**
 * Fonction de navigation entre onglets
 */
function mainTabNavigation(){
	/** Navigation entre onglet */
	$("div.box-tab").click(
		function(){
			var tabId = $(this).attr("id");
			
			/** activation/desactivation onglet */
			$("section.content").find("div.box-tab").removeClass("active");
			$("section.content").find("#"+tabId).addClass("active");
			
			/** Navigation article */
			$("section.content").find("div.article-content").removeClass("active");
			$("section.content").find("#content-" + tabId).addClass("active");
			
			/**Navigation propriete */
			$("section.content").find("div.activity-content").removeClass("active");
			$("section.content").find("#activity-" + tabId).addClass("active");
						
			/**Navigation activit� */
			$("section.content").find("div.property-content").removeClass("active");
			$("section.content").find("#property-" + tabId).addClass("active");
		}
	);
	
	/** Fermeture onglet */
	$("button.close-btn-box-tab").click(
		function(){
			var tabId = $(this).parent().attr("id");
			$("section.content").find("#"+tabId).remove();
			$("section.content").find("#content-" + tabId).remove();
			$("section.content").find("#activity-" + tabId).remove();
			$("section.content").find("#property-" + tabId).remove();
			
			$("#home-tab").click();
		}
	);
}

/**
 * Fonction de gestion du tableau liste des documents
 */
function manageFilesListArticleContent(){
	$("table.table-files-list-item-article-content").DataTable({
		"language": {
            "url": "js/plugin/datatables/French.json"
        },
        "scrollY":        "auto",
        "scrollCollapse": true,
        "paging":         false,
        "autoWidth": false,
        "columnDefs": [
                       { "orderable": false, "width": "0px", "targets": 0 }
                     ],
        "order": [[ 1, "desc" ]]
    });
}